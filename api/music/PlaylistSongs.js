const HttpCommand = require("../../models/HttpCommand");
const Utils = require("../../Utils");
const BotService = require("../../services/BotService");

class PlaylistSongs extends HttpCommand {

    constructor() {
        super();
        this.name = 'playlist/songs';
        this.permissions = ['music'];
    }

    async execute(req, res, httpServer) {
        this.retrieveInfos(req, res, httpServer, async infos => {
            const {member, bot, message, args} = infos;

            if (!member.voice.channel) return Utils.error(res, 'memberVoiceNotConnected');

            const playlistId = args[0];
            const songs = await BotService.getSongs(playlistId);


            res.json({
                code: 200,
                songs
            })
        })
    }

}

module.exports = PlaylistSongs;