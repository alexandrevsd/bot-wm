const HttpCommand = require("../../models/HttpCommand");
const Utils = require("../../Utils");
const BotService = require("../../services/BotService");

class PlaylistEdit extends HttpCommand {

    constructor() {
        super();
        this.name = 'playlist_edit';
        this.permissions = ['music'];
    }

    async execute(req, res, httpServer) {
        this.retrieveInfos(req, res, httpServer, async infos => {
            const {member, bot, message, args} = infos;

            if (!member.voice.channel) return Utils.error(res, 'memberVoiceNotConnected');
            if (args.length === 0) return Utils.error(res, 'noNameSent');
            if (args.length === 1) return Utils.error(res, 'noPublicValueSent');

            const name = args[0];
            const publicValue = args[1];
            const id = args[2];
            let playlist;

            if(id) {
                playlist = await BotService.getPlaylist(bot.id, id);
                playlist.title = name;
                playlist.public = publicValue;
                await BotService.savePlaylist(playlist);
                console.log('edit')
            } else {
                playlist = await BotService.createPlaylist(bot.id, member.id, name, publicValue, 0);
            }

            bot.playlists.set(playlist.id, playlist);

            res.json({
                code: 200,
                playlist
            })
        })
    }

}

module.exports = PlaylistEdit;