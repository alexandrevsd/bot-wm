const HttpCommand = require("../../models/HttpCommand");
const Utils = require("../../Utils");
const Song = require("../../models/Song");
const BotService = require("../../services/BotService");

class QueueAddPlaylist extends HttpCommand {

    constructor() {
        super();
        this.name = 'queue/addPlaylist';
        this.permissions = ['music'];
    }

    async execute(req, res, httpServer) {
        this.retrieveInfos(req, res, httpServer, async infos => {
            const {member, bot, args} = infos;

            if(!member.voice.channel) return Utils.error(res, 'memberVoiceNotConnected');

            const queue = bot.voice.queue;

            const playlistId = args[0];
            const songs = await BotService.getSongs(playlistId);

            queue.addSongs(songs);
            let queueToSend = queue;

            if(bot.voice.random) {
                const randomQueue = bot.voice.randomQueue;
                randomQueue.addSongs(songs);
                queueToSend = randomQueue;
            }

            httpServer.app.socketServer.emit(bot.id + 'queue', {queue: queueToSend});

            res.json({
                code: 200
            })
        })
    }

}

module.exports = QueueAddPlaylist;