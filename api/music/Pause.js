const HttpCommand = require("../../models/HttpCommand");
const Utils = require("../../Utils");
const Song = require('../../models/Song');
const ExternalPlaylist = require("../../models/ServicePlaylist");

class Pause extends HttpCommand {

    constructor() {
        super();
        this.name = 'pause';
        this.permissions = ['music'];
    }

    async execute(req, res, httpServer) {
        this.retrieveInfos(req, res, httpServer, async infos => {
            const {member, bot} = infos;

            if(!member.voice.channel) return Utils.error(res, 'memberVoiceNotConnected');
            if(!bot.voice.isConnected()) return Utils.error(res, 'botVoiceNotConnected');
            if(!bot.voice.playing) return Utils.error(res, 'musicNotPlaying');
            if(bot.voice.paused) return Utils.error(res, 'alreadyPaused');

            await bot.voice.pause();

            res.json({
                code: 200
            })
        })
    }

}

module.exports = Pause;