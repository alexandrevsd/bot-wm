const HttpCommand = require("../../models/HttpCommand");
const Utils = require("../../Utils");

class QueueSave extends HttpCommand {

    constructor() {
        super();
        this.name = 'queue_save';
        this.permissions = ['music'];
    }

    async execute(req, res, httpServer) {
        this.retrieveInfos(req, res, httpServer, async infos => {
            const {member, bot, args} = infos;

            if(!member.voice.channel) return Utils.error(res, 'memberVoiceNotConnected');
            if(args.length === 0 || (!args[0].position && !args[0].songs)) return Utils.error(res, 'noQueueSent');

            const queue = bot.voice.random ? bot.voice.randomQueue : bot.voice.queue;

            queue.position = args[0].position;
            queue.songs = args[0].songs;

            httpServer.app.socketServer.emit(bot.id + 'queue', {queue});

            res.json({
                code: 200
            })
        })
    }

}

module.exports = QueueSave;