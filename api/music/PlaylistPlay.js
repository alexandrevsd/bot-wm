const HttpCommand = require("../../models/HttpCommand");
const Utils = require("../../Utils");
const Song = require("../../models/Song");
const BotService = require("../../services/BotService");

class PlaylistPlay extends HttpCommand {

    constructor() {
        super();
        this.name = 'playlist/play';
        this.permissions = ['music'];
    }

    async execute(req, res, httpServer) {
        this.retrieveInfos(req, res, httpServer, async infos => {
            const {member, bot, message, args} = infos;

            if (!member.voice.channel) return Utils.error(res, 'memberVoiceNotConnected');

            const playlistId = args[0];
            const songs = await BotService.getSongs(playlistId);

            const actualQueue = bot.voice.random ? bot.voice.randomQueue : bot.voice.queue;
            const otherQueue = bot.voice.random ? bot.voice.queue : bot.voice.randomQueue;

            const firstPart = actualQueue.songs.slice(0, actualQueue.position);
            const secondPart = actualQueue.songs.slice(actualQueue.position, actualQueue.songs.length - 1);
            actualQueue.songs = [...firstPart, ...songs, ...secondPart];

            otherQueue.addSongs(songs);

            bot.app.socketServer.emit(bot.id + 'queue', actualQueue);

            if (!bot.voice.isConnected()) await bot.voice.join(member.voice.channel);
            await bot.voice.playQueue(message, true);

            res.json({
                code: 200
            })
        })
    }

}

module.exports = PlaylistPlay;