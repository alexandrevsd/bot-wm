const HttpCommand = require("../../models/HttpCommand");
const Utils = require("../../Utils");
const BotService = require("../../services/BotService");

class PlaylistSave extends HttpCommand {

    constructor() {
        super();
        this.name = 'playlist/save';
        this.permissions = ['music'];
    }

    async execute(req, res, httpServer) {
        this.retrieveInfos(req, res, httpServer, async infos => {
            const {member, bot, args} = infos;

            if(!member.voice.channel) return Utils.error(res, 'memberVoiceNotConnected');

            const playlistId = args[0];
            const songs = args[1];
            const playlist = await BotService.getPlaylist(bot.id, playlistId);
            if(!playlist) return Utils.error(res, 'playlistNotFound');
            if(member.id !== playlist.ownerId) return Utils.error(res, 'notPlaylistOwner');

            let index = 0;
            for (const song of songs) {
                await BotService.setSongIndex(song.id, index);
                index++;
            }

            res.json({
                code: 200
            })
        })
    }

}

module.exports = PlaylistSave;