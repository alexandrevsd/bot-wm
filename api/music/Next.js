const HttpCommand = require("../../models/HttpCommand");
const Utils = require("../../Utils");

class Next extends HttpCommand {

    constructor() {
        super();
        this.name = 'next';
        this.permissions = ['music'];
    }

    async execute(req, res, httpServer) {
        this.retrieveInfos(req, res, httpServer, async infos => {
            const {member, bot, message} = infos;

            if(!member.voice.channel) return Utils.error(res, 'memberVoiceNotConnected');
            if(!bot.voice.isConnected()) return Utils.error(res, 'botVoiceNotConnected');
            if(!bot.voice.playing) return Utils.error(res, 'musicNotPlaying');

            const queue = bot.voice.random ? bot.voice.randomQueue : bot.voice.queue;

            if(!queue.hasNextSong() && !bot.voice.loop) return Utils.error(res, 'noNextMusic');

            queue.next();

            await bot.voice.playQueue(message, true);


            res.json({
                code: 200,
                song: queue.getCurrentSong()
            })
        })
    }

}

module.exports = Next;