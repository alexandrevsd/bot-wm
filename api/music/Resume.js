const HttpCommand = require("../../models/HttpCommand");
const Utils = require("../../Utils");

class Resume extends HttpCommand {

    constructor() {
        super();
        this.name = 'resume';
        this.permissions = ['music'];
    }

    async execute(req, res, httpServer) {
        this.retrieveInfos(req, res, httpServer, async infos => {
            const {member, bot} = infos;

            if(!member.voice.channel) return Utils.error(res, 'memberVoiceNotConnected');
            if(!bot.voice.isConnected()) return Utils.error(res, 'botVoiceNotConnected');
            if(!bot.voice.playing) return Utils.error(res, 'musicNotPlaying');
            if(!bot.voice.paused) return Utils.error(res, 'musicNotPaused');

            await bot.voice.resume();

            res.json({
                code: 200
            })
        })
    }

}

module.exports = Resume;