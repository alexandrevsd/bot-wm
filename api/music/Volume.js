const HttpCommand = require("../../models/HttpCommand");
const Utils = require("../../Utils");

class Volume extends HttpCommand {

    constructor() {
        super();
        this.name = 'volume';
        this.permissions = ['music'];
    }

    async execute(req, res, httpServer) {
        this.retrieveInfos(req, res, httpServer, async infos => {
            const {member, bot, args} = infos;
            const volume = args[0];

            if(!member.voice.channel) return Utils.error(res, 'memberVoiceNotConnected');
            if(!(volume >= 0 && volume <= 100)) return Utils.error(res, 'volumeNotCorrect');

            bot.voice.setVolume(volume);

            res.json({
                code: 200,
                volume
            })
        })
    }

}

module.exports = Volume;