const HttpCommand = require("../../models/HttpCommand");
const Utils = require("../../Utils");
const MusicService = require("../../services/MusicService");

class Search extends HttpCommand {

    constructor() {
        super();
        this.name = 'search';
        this.permissions = ['music'];
    }

    async execute(req, res, httpServer) {
        this.retrieveInfos(req, res, httpServer, async infos => {
            const {member, bot, args, message} = infos;
            let haveToPlay, song;

            if (args.length === 0) return Utils.error(res, 'noSearchQuery');

            if (isNaN(args[0]) || args.length > 1) {

                if (args[0] === 'yt') args[0] = 'youtube';
                if (args[0] === 'sc') args[0] = 'soundcloud';
                const service = args[0] === 'youtube' || args[0] === 'vk' || args[0] === 'soundcloud' ? args.shift() : 'youtube';
                const query = args.join(' ');
                await bot.voice.search.set(service, query, 10);

            } else {

                if (!member.voice.channel) return Utils.error(res, 'memberVoiceNotConnected');
                if (!bot.voice.isConnected()) await bot.voice.join(member.voice.channel);

                const index = args[0] - 1;
                song = bot.voice.search.getSong(index);
                if (bot.voice.search.songs.length > index) bot.voice.queue.addSong(song);
                else return Utils.error(res, 'searchResultNotFound');
                haveToPlay = !bot.voice.playing;
                if (haveToPlay) await bot.voice.playQueue(message, true);
            }

            res.json({
                code: 200,
                search: bot.voice.search.songs,
                song,
                played: haveToPlay
            })
        })
    }

}

module.exports = Search;