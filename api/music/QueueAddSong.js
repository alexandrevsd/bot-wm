const HttpCommand = require("../../models/HttpCommand");
const Utils = require("../../Utils");
const Song = require("../../models/Song");

class QueueAddSong extends HttpCommand {

    constructor() {
        super();
        this.name = 'queue_add_song';
        this.permissions = ['music'];
    }

    async execute(req, res, httpServer) {
        this.retrieveInfos(req, res, httpServer, async infos => {
            const {member, bot, args} = infos;

            if(!member.voice.channel) return Utils.error(res, 'memberVoiceNotConnected');

            const queue = bot.voice.queue;

            const song = new Song({url:args[0]});
            if(!await song.doesExists()) return Utils.error(res, 'songDoesNotExists');
            queue.addSong(song);
            let queueToSend = queue;
            if(bot.voice.random) {
                const randomQueue = bot.voice.randomQueue;
                randomQueue.addSong(song);
                queueToSend = randomQueue;
            }

            httpServer.app.socketServer.emit(bot.id + 'queue', {queue: queueToSend});

            res.json({
                code: 200
            })
        })
    }

}

module.exports = QueueAddSong;