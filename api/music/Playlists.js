const HttpCommand = require("../../models/HttpCommand");
const Utils = require("../../Utils");
const Song = require("../../models/Song");

class Playlists extends HttpCommand {

    constructor() {
        super();
        this.name = 'playlists';
        this.permissions = ['music'];
    }

    async execute(req, res, httpServer) {
        this.retrieveInfos(req, res, httpServer, async infos => {
            const {member, bot, message, args} = infos;

            if (!member.voice.channel) return Utils.error(res, 'memberVoiceNotConnected');

            let playlists = [];

            if(args.length > 0 && args[0] === 'owned') {
                bot.playlists.forEach(playlist => {
                    if(playlist.ownerId === member.id) playlists.push(playlist);
                });
            } else {
                bot.playlists.forEach(playlist => {
                    if(playlist.public && playlist.ownerId !== member.id) {
                        playlist.ownerUsername = bot.getGuild().members.cache.get(playlist.ownerId).user.username;
                        playlists.push(playlist);
                    }
                });
            }

            res.json({
                code: 200,
                playlists
            })
        })
    }

}

module.exports = Playlists;