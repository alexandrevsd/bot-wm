const HttpCommand = require("../../models/HttpCommand");
const Utils = require("../../Utils");

class Shuffle extends HttpCommand {

    constructor() {
        super();
        this.name = 'shuffle';
        this.permissions = ['music'];
    }

    async execute(req, res, httpServer) {
        this.retrieveInfos(req, res, httpServer, async infos => {
            const {member, bot} = infos;

            if(!member.voice.channel) return Utils.error(res, 'memberVoiceNotConnected');
            if(!bot.voice.isConnected()) return Utils.error(res, 'botVoiceNotConnected');
            if(!bot.voice.playing) return Utils.error(res, 'musicNotPlaying');


            bot.voice.random = !bot.voice.random;
            if(bot.voice.random) {
                bot.voice.randomQueue.init(bot.voice.queue, bot.voice.playing);
            }


            res.json({
                code: 200,
                random: bot.voice.random
            })
        })
    }

}

module.exports = Shuffle;