const HttpCommand = require("../../models/HttpCommand");
const Utils = require("../../Utils");
const lyricsParse = require('lyrics-parse');
const GoogleLyricsService = require("../../services/GoogleLyricsService");
const LyricsService = require("../../services/LyricsService");

class Lyrics extends HttpCommand {

    constructor() {
        super();
        this.name = 'lyrics';
        this.permissions = ['music'];
    }

    async execute(req, res, httpServer) {
        this.retrieveInfos(req, res, httpServer, async infos => {
            const {member, bot} = infos;

            if (!member.voice.channel) return Utils.error(res, 'memberVoiceNotConnected');
            if (!bot.voice.isConnected()) return Utils.error(res, 'botVoiceNotConnected');
            if (!bot.voice.playing) return Utils.error(res, 'musicNotPlaying');

            const {artist, title} = bot.voice.queue.getCurrentSong();

            /*try {
                const searches = await genius.songs.search(`${title}`);
                const results = [];
                searches.forEach(result => {
                    if(result.artist.name.toLowerCase().includes(artist.toLowerCase())) {
                        results.push(result);
                    }
                })
                const firstSong = results[0];
                lyrics = await firstSong.lyrics();
            }
            catch {
                return Utils.error(res, 'geniusError');
            }*/

            /*let lyrics = await GoogleLyricsService.get(title);
            if(lyrics === undefined) {
                lyrics = await GeniusService.get(title);
            }*/

            let lyrics = await LyricsService.get(title, artist);

            if(lyrics === undefined) return Utils.error(res, 'noLyricsFound');

            const song = bot.voice.queue.getCurrentSong();

            res.json({
                code: 200,
                lyrics,
                song
            })
        })
    }

}

module.exports = Lyrics;