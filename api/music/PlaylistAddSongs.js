const HttpCommand = require("../../models/HttpCommand");
const Utils = require("../../Utils");
const BotService = require("../../services/BotService");

class PlaylistAddSongs extends HttpCommand {

    constructor() {
        super();
        this.name = 'playlist_add_songs';
        this.permissions = ['music'];
    }

    async execute(req, res, httpServer) {
        this.retrieveInfos(req, res, httpServer, async infos => {
            const {member, bot, args} = infos;

            if(args.length < 2) return Utils.error(res, 'notEnoughArgs');

            const playlistId = args[0];
            const songs = args[1];
            let playlist = await BotService.getPlaylist(bot.id, playlistId);

            if(!playlist) return Utils.error(res, 'playlistNotFound');
            if(member.id !== playlist.ownerId) return Utils.error(res, 'notPlaylistOwner');

            let lengthToAdd = 0;
            for (const song of songs) {
                await BotService.createSong(playlist.id, song);
                lengthToAdd += song.length;
            }

            playlist.length += lengthToAdd;
            playlist = await BotService.savePlaylist(playlist);
            bot.playlists.set(playlist.id, playlist);

            res.json({
                code: 200
            })
        })
    }

}

module.exports = PlaylistAddSongs;