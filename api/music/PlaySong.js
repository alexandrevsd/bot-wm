const HttpCommand = require("../../models/HttpCommand");
const Utils = require("../../Utils");
const Song = require("../../models/Song");

class PlaySong extends HttpCommand {

    constructor() {
        super();
        this.name = 'play_song';
        this.permissions = ['music'];
    }

    async execute(req, res, httpServer) {
        this.retrieveInfos(req, res, httpServer, async infos => {
            const {member, bot, message, args} = infos;

            if (!member.voice.channel) return Utils.error(res, 'memberVoiceNotConnected');

            const queue = bot.voice.random ? bot.voice.randomQueue : bot.voice.queue;

            const song = new Song({url: args[0]});
            if (!await song.doesExists()) return Utils.error(res, 'songDoesNotExists');
            const firstPart = queue.songs.slice(0, queue.position);
            const secondPart = queue.songs.slice(queue.position, queue.songs.length - 1);
            queue.songs = [...firstPart, song, ...secondPart];

            if (!bot.voice.isConnected()) await bot.voice.join(member.voice.channel);
            await bot.voice.playQueue(message, true);


            res.json({
                code: 200
            })
        })
    }

}

module.exports = PlaySong;