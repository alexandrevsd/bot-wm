const HttpCommand = require("../../models/HttpCommand");
const Utils = require("../../Utils");
const Song = require("../../models/Song");
const ServicePlaylist = require("../../models/ServicePlaylist");

class Retrieve extends HttpCommand {

    constructor() {
        super();
        this.name = 'retrieve';
        this.permissions = ['music'];
    }

    async execute(req, res, httpServer) {
        this.retrieveInfos(req, res, httpServer, async infos => {
            const {member, bot, args} = infos;

            if (args.length === 0) return Utils.error(res, 'noArgsSent');

            let urlList = [], urlPlaylist, playlistsList = [], youtubeList = [], soundcloudList = [];
            const searchQuery = args.join(' ');

            if (Utils.isUrl(args[0])) {
                const {service, type} = await Utils.musicLinkParser(args[0]);
                if (type === 'song') {
                    const song = new Song({url: args[0]});
                    if (await song.doesExists()) urlList.push(song);
                } else {
                    if (service === 'youtube' || service === 'soundcloud') {
                        const playlist = new ServicePlaylist(service, args[0]);
                        if (await playlist.doesExists()) urlPlaylist = playlist;
                    } else return Utils.error(res, 'notYoutubeOrSoundcloudPlaylist')
                }
            } else {
                bot.playlists.forEach(playlist => {
                    if (playlist.title.toLowerCase().includes(searchQuery.toLowerCase())) {
                        playlistsList.push(playlist);
                    }
                });
                const search = async (service) => {
                    const result = await Utils.api('/music/search', bot, member.id, [service, ...args]);
                    if (result.code === 200) {
                        return result.search;
                    }
                    return [];
                }
                youtubeList = await search('youtube');
                soundcloudList = await search('soundcloud');
            }

            res.json({
                code: 200,
                urlList,
                urlPlaylist,
                playlistsList,
                youtubeList,
                soundcloudList
            })
        })
    }

}

module.exports = Retrieve;