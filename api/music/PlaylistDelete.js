const HttpCommand = require("../../models/HttpCommand");
const Utils = require("../../Utils");
const BotService = require("../../services/BotService");

class PlaylistDelete extends HttpCommand {

    constructor() {
        super();
        this.name = 'playlist/delete';
        this.permissions = ['music'];
    }

    async execute(req, res, httpServer) {
        this.retrieveInfos(req, res, httpServer, async infos => {
            const {member, bot, message, args} = infos;

            if (!member.voice.channel) return Utils.error(res, 'memberVoiceNotConnected');

            console.log('ok')

            const id = args[0];
            const playlist = await BotService.getPlaylist(bot.id, id);
            bot.playlists.delete(playlist.id);
            await BotService.deletePlaylist(playlist.id);

            res.json({
                code: 200,
                playlist
            })
        })
    }

}

module.exports = PlaylistDelete;