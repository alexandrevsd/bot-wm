const HttpCommand = require("../../models/HttpCommand");
const Utils = require("../../Utils");

class Stop extends HttpCommand {

    constructor() {
        super();
        this.name = 'stop';
        this.permissions = ['music'];
    }

    async execute(req, res, httpServer) {
        this.retrieveInfos(req, res, httpServer, async infos => {
            const {member, bot} = infos;

            if(!member.voice.channel) return Utils.error(res, 'memberVoiceNotConnected');
            if(!bot.voice.isConnected()) return Utils.error(res, 'botVoiceNotConnected');
            if(!bot.voice.playing) return Utils.error(res, 'musicNotPlaying');

            await bot.voice.stop();

            res.json({
                code: 200,
                song: bot.voice.queue.getCurrentSong()
            })
        })
    }

}

module.exports = Stop;