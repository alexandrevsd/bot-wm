const HttpCommand = require("../../models/HttpCommand");
const Utils = require("../../Utils");

class Seek extends HttpCommand {

    constructor() {
        super();
        this.name = 'seek';
        this.permissions = ['music'];
    }

    async execute(req, res, httpServer) {
        this.retrieveInfos(req, res, httpServer, async infos => {
            const {member, bot, args, message} = infos;

            if (!member.voice.channel) return Utils.error(res, 'memberVoiceNotConnected');
            if (!bot.voice.isConnected()) return Utils.error(res, 'botVoiceNotConnected');
            if (!bot.voice.playing) return Utils.error(res, 'musicNotPlaying');
            if (isNaN(args[0])) return Utils.error(res, 'seekNotANumber');

            let seconds = parseInt(args[0]);
            let currentSong = await bot.voice.queue.getCurrentSong();
            let toSeek = bot.voice.getElapsedTime() + seconds;

            if (toSeek < 0) toSeek = 0;
            if (toSeek > currentSong.length) toSeek = currentSong.length;

            bot.voice.toSeek = toSeek;
            await bot.voice.playQueue(message, true);

            res.json({
                code: 200,
                seeked: seconds
            })
        })
    }

}

module.exports = Seek;