const HttpCommand = require("../../models/HttpCommand");
const Utils = require("../../Utils");

class Loop extends HttpCommand {

    constructor() {
        super();
        this.name = 'loop';
        this.permissions = ['music'];
    }

    async execute(req, res, httpServer) {
        this.retrieveInfos(req, res, httpServer, async infos => {
            const {member, bot} = infos;

            if(!member.voice.channel) return Utils.error(res, 'memberVoiceNotConnected');
            if(!bot.voice.isConnected()) return Utils.error(res, 'botVoiceNotConnected');
            if(!bot.voice.playing) return Utils.error(res, 'musicNotPlaying');


            bot.voice.loop = !bot.voice.loop;


            res.json({
                code: 200,
                loop: bot.voice.loop
            })
        })
    }

}

module.exports = Loop;