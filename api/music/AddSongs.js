const HttpCommand = require("../../models/HttpCommand");
const Utils = require("../../Utils");
const Song = require("../../models/Song");

class AddSongs extends HttpCommand {

    constructor() {
        super();
        this.name = 'add_songs';
        this.permissions = ['music'];
    }

    async execute(req, res, httpServer) {
        this.retrieveInfos(req, res, httpServer, async infos => {
            const {member, bot, args} = infos;

            if(!member.voice.channel) return Utils.error(res, 'memberVoiceNotConnected');

            const queue = bot.voice.queue;

            const songs = args[0];
            queue.addSongs(songs);
            if(bot.voice.random) bot.voice.randomQueue.addSongs(songs);
            if(!bot.voice.playing) {
                if (!bot.voice.isConnected()) await bot.voice.join(member.voice.channel);
                await bot.voice.playQueue(undefined, true);
            }

            res.json({
                code: 200
            })
        })
    }

}

module.exports = AddSongs;