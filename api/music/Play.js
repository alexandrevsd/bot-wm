const HttpCommand = require("../../models/HttpCommand");
const Utils = require("../../Utils");
const Song = require('../../models/Song');
const ServicePlaylist = require("../../models/ServicePlaylist");
const BotService = require("../../services/BotService");

class Play extends HttpCommand {

    constructor() {
        super();
        this.name = 'play';
        this.permissions = ['music'];
    }

    async execute(req, res, httpServer) {
        this.retrieveInfos(req, res, httpServer, async infos => {
            const {member, bot, args, message} = infos;
            if(!member.voice.channel) return Utils.error(res, 'memberVoiceNotConnected');
            if(!bot.voice.isConnected()) await bot.voice.join(member.voice.channel);

            const haveToPlay = !bot.voice.playing;
            const {service, type} = await Utils.musicLinkParser(args[0]);
            let song, playlist;

            if(Utils.isUrl(args[0])) {
                if(type === 'song') {
                    song = new Song({url:args[0]});
                    if(!await song.doesExists()) return Utils.error(res, 'songDoesNotExists');
                    bot.voice.queue.addSong(song);
                } else {
                    if(service === 'youtube' || service === 'soundcloud') {
                        playlist = new ServicePlaylist(service, args[0]);
                        if(!await playlist.doesExists()) return Utils.error(res, 'playlistDoesNotExists');
                        await bot.voice.queue.addPlaylist(playlist);
                        if(haveToPlay) song = bot.voice.queue.getCurrentSong();
                    } else return Utils.error(res, 'notYoutubeOrSoundcloudPlaylist')
                }
            } else {
                const playlistName = args.join(' ').toLowerCase();
                if(bot.playlists.has(playlistName)) {
                    playlist = bot.playlists.get(playlistName);
                    bot.voice.queue.addSongs(await BotService.getSongs(playlist.id));
                    if(haveToPlay) song = bot.voice.queue.getCurrentSong();
                } else return Utils.error(res, 'playlistDoesNotExists');
            }


            if(haveToPlay) await bot.voice.playQueue(message, true);

            res.json({
                code: 200,
                played: haveToPlay,
                playlist: playlist,
                song
            })
        })
    }

}

module.exports = Play;