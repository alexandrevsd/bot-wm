const HttpCommand = require("../../models/HttpCommand");
const Utils = require("../../Utils");

class QueueDeletePosition extends HttpCommand {

    constructor() {
        super();
        this.name = 'queue_delete_position';
        this.permissions = ['music'];
    }

    async execute(req, res, httpServer) {
        this.retrieveInfos(req, res, httpServer, async infos => {
            const {member, bot, args} = infos;

            if(!member.voice.channel) return Utils.error(res, 'memberVoiceNotConnected');
            if(args.length === 0) return Utils.error(res, 'noPositionSent');

            const queue = bot.voice.random ? bot.voice.randomQueue : bot.voice.queue;
            const positionToDelete = args[0];

            if(queue.songs.length <= positionToDelete) return Utils.error(res, 'noSongAtThisPosition');

            queue.songs.splice(positionToDelete, 1);

            if(queue.position === positionToDelete) {
                await bot.voice.playQueue(undefined, true);
            }

            if(positionToDelete < queue.position) {
                queue.position--;
            }

            httpServer.app.socketServer.emit(bot.id + 'queue', {queue});

            res.json({
                code: 200
            })
        })
    }

}

module.exports = QueueDeletePosition;