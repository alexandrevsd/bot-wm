const HttpCommand = require("../../models/HttpCommand");
const Utils = require("../../Utils");
const BotService = require("../../services/BotService");

class Playlist extends HttpCommand {

    constructor() {
        super();
        this.name = 'playlist';
        this.permissions = ['music'];
    }

    async execute(req, res, httpServer) {
        this.retrieveInfos(req, res, httpServer, async infos => {
            const {member, bot, message, args} = infos;

            if (!member.voice.channel) return Utils.error(res, 'memberVoiceNotConnected');

            const playlistId = args[0];
            const playlist = await BotService.getPlaylist(bot.id, playlistId);
            playlist.ownerUsername = bot.getGuild().members.cache.get(playlist.ownerId).user.username;


            res.json({
                code: 200,
                playlist
            })
        })
    }

}

module.exports = Playlist;