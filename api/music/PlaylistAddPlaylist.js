const HttpCommand = require("../../models/HttpCommand");
const Utils = require("../../Utils");
const BotService = require("../../services/BotService");

class PlaylistAddPlaylist extends HttpCommand {

    constructor() {
        super();
        this.name = 'playlist/addPlaylist';
        this.permissions = ['music'];
    }

    async execute(req, res, httpServer) {
        this.retrieveInfos(req, res, httpServer, async infos => {
            const {member, bot, args} = infos;

            if(args.length < 2) return Utils.error(res, 'notEnoughArgs');

            const fromPlaylistId = args[0];
            const toPlaylistId = args[1];
            const songs = await BotService.getSongs(fromPlaylistId);

            let playlist = await BotService.getPlaylist(bot.id, toPlaylistId);
            if(!playlist) return Utils.error(res, 'playlistNotFound');
            if(member.id !== playlist.ownerId) return Utils.error(res, 'notPlaylistOwner');

            let lengthToAdd = 0;
            for (const song of songs) {
                await BotService.createSong(playlist.id, song);
                lengthToAdd += song.length;
            }

            console.log(playlist.id)
            playlist.length += lengthToAdd;
            playlist = await BotService.savePlaylist(playlist);
            bot.playlists.set(playlist.id, playlist);
            console.log(playlist)

            res.json({
                code: 200
            })
        })
    }

}

module.exports = PlaylistAddPlaylist;