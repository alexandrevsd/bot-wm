const HttpCommand = require("../../models/HttpCommand");
const Utils = require("../../Utils");

class QueuePlayPosition extends HttpCommand {

    constructor() {
        super();
        this.name = 'queue_play_position';
        this.permissions = ['music'];
    }

    async execute(req, res, httpServer) {
        this.retrieveInfos(req, res, httpServer, async infos => {
            const {member, bot, args} = infos;

            if(!member.voice.channel) return Utils.error(res, 'memberVoiceNotConnected');
            if(args.length === 0) return Utils.error(res, 'noPositionSent');

            const queue = bot.voice.random ? bot.voice.randomQueue : bot.voice.queue;
            const newPosition = args[0];

            if(queue.songs.length <= newPosition) return Utils.error(res, 'noSongAtThisPosition');

            queue.position = newPosition;
            if(bot.voice.paused) bot.voice.paused = false;
            await bot.voice.playQueue(undefined, true);
            httpServer.app.socketServer.emit(bot.id + 'queue', {queue});

            res.json({
                code: 200
            })
        })
    }

}

module.exports = QueuePlayPosition;