const HttpCommand = require("../../models/HttpCommand");
const Utils = require("../../Utils");
const BotService = require("../../services/BotService");

class Config extends HttpCommand {

    constructor() {
        super();
        this.name = 'config';
        this.permissions = ['config'];
    }

    async execute(req, res, httpServer) {
        this.retrieveInfos(req, res, httpServer, async infos => {
            const {args, bot} = infos;

            if (args.length < 2) return Utils.error(res, 'notEnoughArgs');

            const parameter = args[0].toLowerCase();
            let value = args[1];
            let parameterName = '';

            if (parameter === 'prefix') {
                if (value.length === 1) {
                    parameterName = 'prefix';
                    bot.config.prefix = value;
                } else return Utils.error(res, 'prefixTooLongOrShort')
            } else if (parameter === 'language') {
                value = value.toUpperCase();
                if (value.length === 2) {
                    parameterName = 'language';
                    bot.config.language = value;
                } else return Utils.error(res, 'languageTooLongOrShort')
            } else if (parameter === 'defaultvolume') {
                value = parseInt(value);
                if (value >= 0 && value <= 100) {
                    parameterName = 'defaultVolume';
                    bot.config.defaultVolume = value;
                } else return Utils.error(res, 'defaultVolumeNotCorrect')
            } else if (parameter === 'deletemessageoncommand') {
                if (value === 'true' || value === 'false') {
                    value = (value === 'true');
                    parameterName = 'deleteMessageOnCommand';
                    bot.config.deleteMessageOnCommand = value;
                } else return Utils.error(res, 'deleteMessageOnCommandNotBoolean')
            } else {
                return Utils.error(res, 'parameterNotFound');
            }

            BotService.saveConfig(bot.config);

            res.json({
                code: 200,
                parameterName,
                value
            })
        })
    }

}

module.exports = Config;