const HttpCommand = require("../../models/HttpCommand");
const ytdl = require('ytdl-core');
const MusicService = require("../../services/MusicService");
const {searchOnMyFreeMp3} = require('vk-music-dl/lib');

class Test extends HttpCommand {

    constructor() {
        super();
        this.name = 'test';
        this.permissions = ['debug'];
    }

    async execute(req, res, httpServer) {
        this.retrieveInfos(req, res, httpServer, async infos => {
            const {args, bot, member} = infos;
            console.log(await searchOnMyFreeMp3("pnl bambina"));

            res.json({
                code: 200
            })
        })
    }

}

module.exports = Test;