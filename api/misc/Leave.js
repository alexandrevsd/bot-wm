const HttpCommand = require("../../models/HttpCommand");
const Utils = require("../../Utils");

class Leave extends HttpCommand {

    constructor() {
        super();
        this.name = 'leave';
        this.permissions = ['move'];
    }

    async execute(req, res, httpServer) {
        this.retrieveInfos(req, res, httpServer, async infos => {
            const {bot} = infos;
            if(!bot.voice.isConnected()) return Utils.error(res, 'noVoiceConnection');
            const channelName = bot.voice.getConnection().channel.name;
            bot.voice.leave();
            res.json({
                code: 200,
                channelName
            })
        })
    }

}

module.exports = Leave;