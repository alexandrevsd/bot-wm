const HttpCommand = require("../../models/HttpCommand");
const Utils = require("../../Utils");

class SearchUser extends HttpCommand {

    constructor() {
        super();
        this.name = 'search_user';
    }

    async execute(req, res, httpServer) {
        const {userId} = req.body;
        const {app} = httpServer;
        let serverId = undefined;
        app.bots.forEach(bot => {
            const voiceChannels = bot.getGuild().channels.cache.filter(channel => channel.type === 'voice');
            voiceChannels.forEach(vc => {
                const voiceChannelMembers = vc.members;
                const foundVoiceChannel = voiceChannelMembers.find(vcm => vcm.user.id === userId);
                if(foundVoiceChannel) {
                    serverId = foundVoiceChannel.guild.id;
                }
            })
        })
        res.json({
            code: 200,
            server: serverId
        })
    }

}

module.exports = SearchUser;