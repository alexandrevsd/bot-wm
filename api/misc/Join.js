const HttpCommand = require("../../models/HttpCommand");
const Utils = require("../../Utils");

// !join
// !join <CHANNEL_ID>

class Join extends HttpCommand {

    constructor() {
        super();
        this.name = 'join';
        this.permissions = ['move'];
    }

    async execute(req, res, httpServer) {
        this.retrieveInfos(req, res, httpServer, async infos => {
            const {member, args, bot} = infos;
            let voiceChannel;
            if(args.length === 0) {
                if(!member.voice.channel) return Utils.error(res, 'memberVoiceNotConnected');
                voiceChannel = member.voice.channel;
            } else {
                const channelsCache = bot.getGuild().channels.cache;
                if(!channelsCache.has(args[0])) return Utils.error(res, 'voiceChannelNotFound');
                voiceChannel = channelsCache.get(args[0]);
            }
            if(bot.voice.isConnected() && voiceChannel.id === bot.voice.getConnection().channel.id) return Utils.error(res, 'alreadyInThisVoiceChannel');
            bot.voice.join(voiceChannel);
            res.json({
                code: 200,
                channelName: voiceChannel.name
            })
        })
    }

}

module.exports = Join;