const HttpCommand = require("../../models/HttpCommand");
const Utils = require("../../Utils");

class FetchBots extends HttpCommand {

    constructor() {
        super();
        this.name = 'fetchBots';
        this.permissions = ['move'];
    }

    async execute(req, res, httpServer) {
        if(!req.body || !req.body.userServers) return Utils.error(res, 'noServersSent');
        const servers = req.body.userServers;
        const serversFound = [];
        servers.forEach(server => {
            if (httpServer.app.bots.has(server)) {
                const bot = httpServer.app.bots.get(server);
                serversFound.push({
                    id: bot.id,
                    name: bot.getGuild().name
                })
            }
        });
        res.json({
            code: 200,
            serversFound
        })
    }

}

module.exports = FetchBots;