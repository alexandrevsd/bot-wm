require('dotenv').config();
const {Client} = require('discord.js');
const {readdirSync} = require('fs');

const SocketServer = require("./SocketServer");
const HttpServer = require("./HttpServer");
const Utils = require("../Utils");
const Bot = require("./Bot");

class App {

    #httpServer;
    socketServer;
    #texts;
    #chatCommands;
    bots;
    client;

    constructor() {
        this.client = new Client();
        this.#httpServer = new HttpServer(this);
        this.#texts = this.#loadTexts();
        this.#chatCommands = this.#loadChatCommands();
        this.bots = new Map();
        this.#connect().then(() => {
            this.#loadBots().then(() => {
                this.#listen();
                this.socketServer = new SocketServer(this);
            });
        });
    }

    #listen = () => {
        this.client.on('message', this.#checkMessage);
        Utils.log('listening messages');
        this.client.on('guildCreate', this.#createBot);
        Utils.log('listening joining guilds');
        this.client.on('voiceStateUpdate', this.#voiceUpdate);
        Utils.log('listening member\'s voices update');
    }

    #connect = async () => {
        await this.client.login(process.env.TOKEN)
        Utils.log(`connected as ${this.client.user.tag}`);
    }

    #voiceUpdate = (oldVoice, newVoice) => {
        if(newVoice.channel) {
            if(oldVoice.channel) {
                if(oldVoice.channel.id !== newVoice.channel.id) {
                    this.socketServer.emit('channel_joined', {
                        userId: newVoice.member.user.id,
                        server: newVoice.guild.id
                    })
                }
            } else {
                this.socketServer.emit('channel_joined', {
                    userId: newVoice.member.user.id,
                    server: newVoice.guild.id
                })
            }
        } else {
            this.socketServer.emit('channel_left', {
                userId: newVoice.member.user.id
            })
        }
    }

    #checkMessage = async (message) => {
        if (message.author.bot) return;
        if (!this.bots.has(message.guild.id)) return;
        const bot = this.bots.get(message.guild.id);
        const prefix = bot.config.prefix;
        if (!message.content.startsWith(prefix)) return;
        const args = message.content.slice(prefix.length).trim().split(/ +/);
        const command = args.shift().toLowerCase();
        if (!this.#chatCommands.has(command)) return;
        this.#chatCommands.get(command).execute(bot, message, args, this.text);
    }

    #loadTexts = () => {
        const texts = new Map();
        Utils.log(`loading all texts...`, 'App', 'red');
        const textsFiles = readdirSync('./resources/texts').filter(file => file.endsWith('.json'));
        for (const file of textsFiles) {
            const text = require(`../resources/texts/${file}`);
            const textLanguage = file.replace('.json', '');
            texts.set(textLanguage, text);
            //Utils.log(`loading "${textLanguage}" texts...`, 'App', 'red');
        }
        Utils.log(`${texts.size} texts loaded !`, 'App', 'red');
        return texts;
    }

    #loadChatCommands = () => {
        const chatCommands = new Map();
        let chatCommandsCounter = 0;
        Utils.log(`loading all chat commands...`, 'App', 'red');
        const commandFolders = readdirSync('./chatCommands');
        for (const folder of commandFolders) {
            const commandFiles = readdirSync(`./chatCommands/${folder}`).filter(file => file.endsWith('.js'));
            for (const file of commandFiles) {
                const command = new (require(`../chatCommands/${folder}/${file}`));
                chatCommands.set(command.name, command);
                chatCommandsCounter++;
                command.aliases.forEach(alias => chatCommands.set(alias, command));
                //Utils.log(`loading "${command.name}" command...`, 'App', 'red');
            }
        }
        Utils.log(`${chatCommandsCounter} chat commands loaded !`, 'App', 'red');
        return chatCommands;
    }

    #createBot = async (guild) => {
        this.bots.set(guild.id, new Bot(this, guild.id));
    }

    #loadBots = async () => {
        Utils.log('generating bots...', 'App', 'red');
        this.client.guilds.cache.each(await this.#createBot);
        Utils.log(`${this.bots.size} bots generated !`, 'App', 'red');
    }
}

module.exports = App;