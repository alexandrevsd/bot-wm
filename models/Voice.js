const ytdl = require('ytdl-core');
const scdl = require("soundcloud-downloader").default;

const Search = require("./Search");
const Queue = require("./Queue");
const RandomQueue = require("./RandomQueue");

class Voice {

    bot;
    search;
    queue;
    randomQueue;
    volume;
    playing;
    paused;
    seeked;
    toSeek;
    random;
    loop;

    constructor(bot) {
        this.bot = bot;
        this.#init();
    }

    async join(voiceChannel) {
        await voiceChannel.join();
    }

    async leave() {
        this.isConnected() && await this.getConnection().disconnect();
    }

    pause() {
        this.paused = true;
        this.getConnection().dispatcher.pause();
        this.bot.app.socketServer.emit(this.bot.id+'pause');
    }

    resume() {
        this.paused = false;
        this.getConnection().dispatcher.resume();
        this.bot.app.socketServer.emit(this.bot.id+'resume', this.getElapsedTime() * 1000);
    }

    async stop() {
        this.#init();
        await this.leave();
        this.bot.app.socketServer.emit(this.bot.id+'stop');
    }

    #init() {
        this.search = new Search();
        this.queue = new Queue();
        this.randomQueue = new RandomQueue();
        this.volume = this.bot.config.defaultVolume;
        this.playing = false;
        this.paused = false;
        this.seeked = 0;
        this.toSeek = 0;
        this.random = false;
        this.loop = false;
    }

    getElapsedTime() {
        return this.isPlaying() ? this.seeked + Math.floor(this.getConnection().dispatcher.streamTime / 1000) : 0;
    }

    getElapsedMs() {
        return this.isPlaying() ? (this.seeked * 1000) + this.getConnection().dispatcher.streamTime : 0;
    }

    async playQueue(message, alreadySent = true) {
        this.playing = true;
        const queue = this.random ? this.randomQueue : this.queue;
        const currentSong = queue.getCurrentSong();
        if (message && !alreadySent) this.bot.send(message, 'J\'ai lancé `' + currentSong.title + '`', ['next_track', 'musical_note']);
        try {
            const player = await this.#getPlayer(currentSong);
            const dispatcher = await this.#play(player);
            dispatcher.on('start', async () => {
                this.bot.app.socketServer.emit(this.bot.id+'song', {song:currentSong, elapsedTime: this.getElapsedTime() * 1000});
            })
            dispatcher.on('finish', async () => {
                await this.#playNextSong(message);
            })
            dispatcher.on('error', async () => {
                await this.#playNextSong(message);
            })
        } catch (e) {
            await this.#playNextSong(message);
        }
    }

    #playNextSong = async (message) => {
        const queue = this.random ? this.randomQueue : this.queue;
        if (queue.hasNextSong()) {
            queue.next();
            await this.playQueue(message, false);
        } else {
            if(this.loop) {
                queue.position = 0;
                await this.playQueue(message, false);
            } else {
                this.playing = false;
                queue.songs = [];
                queue.position = 0;
                this.bot.app.socketServer.emit(this.bot.id+'stop');
            }
        }
    }

    #play = async (player) => {
        const dispatcher = await this.getConnection().play(player, {
            seek: this.toSeek,
            bitrate: 48,
            highWaterMark: 1,
            fec: true
        });
        dispatcher.setVolume(this.volume / 100);
        this.paused = false;
        this.#resetSeek();
        return dispatcher;
    }

    #resetSeek = () => {
        this.seeked = 0
        if (this.toSeek !== 0) {
            this.seeked = this.toSeek;
            this.toSeek = 0;
        }
    }

    #getPlayer = async (song) => {
        let player = '';
        if (song.service === 'youtube') {
            player = await ytdl(song.url, {
                filter: 'audioonly',
                quality: 'lowestaudio'
            });
        } else if(song.service === 'soundcloud') {
            player = await scdl.download(song.url);
        } else if(song.service === 'vk') {
            player = song.url.split('&long')[0];
            /*const searchItems = await searchOnMyFreeMp3(song.artist + ' ' + song.title) || [];
            searchItems.forEach(itemFound => {
                if (itemFound.id === parseInt(song.url)) {
                    player = itemFound.url.split('&long')[0];
                }
            })*/
        }
        return player;
    }

    isConnected() {
        return this.bot.getGuild().voice && this.getConnection() !== null;
    }

    isPlaying() {
        return this.isConnected() && this.playing;
    }

    getConnection() {
        return this.bot.getGuild().voice.connection;
    }

    setVolume(volume) {
        this.volume = volume;
        if(this.isConnected() && this.playing) this.getConnection().dispatcher.setVolume(this.volume / 100);
    }


}

module.exports = Voice;