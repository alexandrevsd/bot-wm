const SongsList = require('./SongsList');

class Playlist extends SongsList {

    id;
    serverId;
    ownerId;
    title;
    public;
    length;
    duration;

    constructor(playlist) {
        super();
        if(playlist) {
            this.id = playlist.id;
            this.serverId = playlist.serverId;
            this.ownerId = playlist.ownerId;
            this.title = playlist.title;
            this.public = playlist.public;
            this.length = playlist.length;
            this.duration = playlist.duration;
        }
    }

}

module.exports = Playlist;