const Queue = require("./Queue");

class RandomQueue extends Queue {

    init(queue, playing = false) {
        let savedSong;
        if(playing) {
            savedSong = queue.songs[queue.position];
            const newQueueFirstPart = queue.songs.slice(0, queue.position);
            const newQueueLastPart = queue.songs.slice(queue.position + 1, queue.songs.length - 1);
            const newQueue = [...newQueueFirstPart, ...newQueueLastPart];
            this.copySongs(newQueue);
        } else {
            this.copySongs(queue.songs);
        }
        this.shuffle();
        if(playing) {
            this.songs = [savedSong, ...this.songs];
        }
    }

    copySongs(songs) {
        this.songs = [...songs];
    }

    shuffle() {
        for (let i = this.songs.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [this.songs[i], this.songs[j]] = [this.songs[j], this.songs[i]];
        }
    }

}

module.exports = RandomQueue;