const cors = require('cors');
const Express = require('express');
const http = require('http');
const socketIo = require('socket.io');

const Utils = require('../Utils');
const Song = require("./Song");

class SocketServer {

    app;
    #server;
    #http;
    #port;
    #io;

    constructor(app) {
        this.app = app;
        this.#port = process.env.SOCKET_PORT || 81;
        this.#server = new Express();
        this.#middlewares();
        this.#http = http.createServer(this.#server);
        this.#io = socketIo(this.#http, {
            cors: {
                origin: process.env.SOCKET_ORIGIN_URL || 'http://localhost:82',
                methods: ['GET', 'POST']
            }
        });
        this.#socketListeners();
        this.#listen();
    }

    #socketListeners = () => {
        this.#io.on('connection', (socket) => {
            socket.on('getPlayer', (serverId) => {
                const bot = this.app.bots.get(serverId);
                const song = (bot.voice.queue.songs.length > 0) ? bot.voice.queue.getCurrentSong() : new Song();
                const length = bot.voice.getElapsedMs();
                this.#io.emit(serverId + 'player', {
                    paused: bot.voice.paused,
                    elapsedTime: bot.voice.getElapsedMs(),
                    playing: bot.voice.playing,
                    random: bot.voice.random,
                    loop: bot.voice.loop,
                    song,
                    length
                });
            })
            socket.on('getQueue', (serverId) => {
                const bot = this.app.bots.get(serverId);
                const queue = bot.voice.random ? bot.voice.randomQueue : bot.voice.queue;
                this.#io.emit(serverId + 'queue', {queue});
            })
            console.log('a user connected');
        });
    }

    emit(action, data) {
        console.log("emit")
        this.#io.emit(action, data);
    }

    #middlewares = () => {
        this.#server.use(cors());
        this.#server.get('/', (req, res) => res.send('ok'));
    }

    #listen = () => {
        this.#http.listen(process.env.SOCKET_PORT, () => {
            Utils.log(`listening at *:${this.#port}`, 'Socket Server', 'yellow');
        });
    }


}

module.exports = SocketServer;