class Permission {

    serverId;
    roleId;
    name;

    constructor(permission) {
        this.serverId = permission.serverId;
        this.roleId = permission.roleId;
        this.name = permission.name;
    }


}

module.exports = Permission;