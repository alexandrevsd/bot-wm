const ytpl = require('ytpl');
const ytsr = require('ytsr');
const scdl = require("soundcloud-downloader").default;
const appleMusic = require("apple-music-playlist");
const {searchOnMyFreeMp3, searchOnVkMusic} = require("vk-music-dl/lib");
const DeezerPublicApi = require('deezer-public-api');
const deezer = new DeezerPublicApi();

const Utils = require("../Utils");
const spotify = new (require('../services/SpotifyService'));
const Playlist = require('./SongsList');
const Song = require("./Song");

class ServicePlaylist extends Playlist {

    name;
    title;
    service;
    url;

    constructor(service, url) {
        super();
        this.service = service;
        this.url = url;
    }

    async doesExists() {
        let exists;
        if (this.service === 'youtube') {
            try {
                const infos = await ytpl(this.url) || {items: []};
                if (infos.items.length > 0) {
                    this.title = infos.title;
                    infos.items.forEach(item => {
                        let song = {
                            service: this.service,
                            title: item.title,
                            artist: item.author.name,
                            url: item.shortUrl,
                            image: item.thumbnails[0].url,
                            length: Utils.getSongLength(this.service, item.durationSec),
                            duration: Utils.getSongDuration(item.durationSec)
                        };
                        this.addSong(new Song(song));
                    })
                    exists = true;
                } else {
                    exists = false;
                }
            } catch {
                exists = false;
            }
        } else if (this.service === 'soundcloud') {
            try {
                const infos = await scdl.getSetInfo(this.url) || {tracks: []};
                if (infos.tracks.length > 0) {
                    this.title = infos.title;
                    infos.tracks.forEach(track => {
                        console.log(track)
                        let song = {
                            image: track.artwork_url,
                            service: this.service,
                            artist: track.user.username,
                            title: track.title,
                            url: track.uri,
                            length: Utils.getSongLength(this.service, track.duration),
                            duration: Utils.getSongDuration(track.duration)
                        };
                        this.addSong(new Song(song));
                    })
                    exists = true;
                } else {
                    exists = false;
                }
            } catch {
                exists = false;
            }
        } else if (this.service === 'spotify') {
            try {
                let songsToSearch = [];
                const tracks = await spotify.getPlaylistTracks(spotify.getPlaylistId(this.url));
                tracks.forEach((track, index) => {
                    track.artist = track.artists[0] ? track.artists[0].name : undefined;
                    track.name = track.name ? track.name.split('(')[0] : undefined;
                    const title = `${track.artist} ${track.name}`;
                    const image = track.album.images[0].url;
                    if (track.artist && track.name) songsToSearch.push({title, image, index});
                })
                const convertedSongs = await this.#convertSongs(songsToSearch);
                convertedSongs.forEach(song => this.addSong(song));
                exists = true;
            } catch (e) {
                console.log(e)
                exists = false;
            }
        } else if (this.service === 'apple_music') {
            try {
                let songsToSearch = [];
                const appleMusicPlaylist = await appleMusic.getPlaylist(this.url);
                appleMusicPlaylist.forEach((track, index) => {
                    if (track.artist && track.title) {
                        const title = track.artist + " " + track.title.split('(')[0];
                        songsToSearch.push({title, image: 'http://vsd.ovh/empty_image.png', index});
                    }
                })
                const convertedSongs = await this.#convertSongs(songsToSearch);
                convertedSongs.forEach(song => this.addSong(song));
                exists = true;
            } catch (e) {
                exists = false;
            }
        } else if (this.service === 'deezer') {
            try {
                let songsToSearch = [];
                const playlistID = Utils.getDeezerPlaylistID(this.url);
                const deezerPlaylist = await deezer.playlist.tracks(playlistID, 200);
                deezerPlaylist["data"].forEach((track, index) => {
                    if (track.artist && track.artist.name && track.title) {
                        const title = track.artist.name + " " + track.title.split('(')[0];
                        songsToSearch.push({title, image, index});
                    }
                })
                const convertedSongs = await this.#convertSongs(songsToSearch);
                convertedSongs.forEach(song => this.addSong(song));
                exists = true;
            } catch (e) {
                exists = false;
            }
        }
        return exists;
    }

    /*
    TODO match score pour meilleurs musiques
    matchScore(spotifyMetas, vkmusicMetas){
        const originalArtistNames = _.map(spotifyMetas.artists, 'name').join(', ').toLowerCase();
        const originalTitle = spotifyMetas.name.toLowerCase();
        const originalDuration = Math.round(spotifyMetas.duration_ms/1000);

        const matchArtistScore = 1 - (leven(originalArtistNames, _.get(vkmusicMetas, 'artist', '').toLowerCase()) / Math.max(originalArtistNames.length, _.get(vkmusicMetas, 'artist', '').length));
        const matchTitleScore =  1 - (leven(originalTitle, _.get(vkmusicMetas, 'title', '').toLowerCase()) / Math.max(originalTitle.length, _.get(vkmusicMetas, 'title', '').length));
        const matchDurationScore = 1 - (Math.abs(originalDuration - _.get(vkmusicMetas, 'duration', 0)) / originalDuration);
        verboseDebug('matchArtistScore=%f matchTitleScore=%f matchDurationScore=%f', matchArtistScore, matchTitleScore, matchDurationScore);
        return matchArtistScore + matchTitleScore + matchDurationScore;
    }*/

    #getVkInfos = async (songToSearch) => {
        let foundResults = await searchOnMyFreeMp3(songToSearch.title);
        console.log(foundResults)
        try {
            let foundResults = await searchOnMyFreeMp3(songToSearch.title);
            console.log(foundResults)
            if (foundResults[0]) foundResults[0].index = songToSearch.index;
            if (foundResults[0]) foundResults[0].title = foundResults[0].title.split("(")[0];
            return foundResults[0];
        } catch {
            return undefined;
        }
    }

    #getYoutubeInfos = async (songToSearch) => {
        try {
            // TODO getYoutubeInfos language change here
            const foundResults = await ytsr(songToSearch.title, {gl: 'FR', hl: 'fr', pages: 1});
            if (foundResults.items[0]) foundResults.items[0].index = songToSearch.index;
            if (foundResults.items[0]) foundResults.items[0].title = songToSearch.title;
            return foundResults.items[0];
        } catch {
            return undefined;
        }
    }

    #convertSong = async (songToSearch) => {
        let convertedSong;
        let foundSong = await this.#getVkInfos(songToSearch);
        let service = 'vk';
        if (foundSong) {
            let image;
            if(foundSong.album && foundSong.album.thumb) {
                if(foundSong.album.thumb.photo_135) {
                    image = foundSong.album.thumb.photo_135;
                } else if(foundSong.album.thumb.photo_270) {
                    image = foundSong.album.thumb.photo_270;
                } else if(foundSong.album.thumb.photo_300) {
                    image = foundSong.album.thumb.photo_300;
                } else if(foundSong.album.thumb.photo_600) {
                    image = foundSong.album.thumb.photo_600;
                } else {
                    image = songToSearch.image;
                }
            } else {
                image = songToSearch.image;
            }
            console.log(foundSong.url)
            convertedSong = {
                service,
                artist: foundSong.artist,
                title: foundSong.title,
                url: foundSong.url,
                length: Utils.getSongLength(service, foundSong.duration),
                index: foundSong.index,
                image
            }
        }
        return convertedSong;
    }

    #convertSongs = async (songsToSearch = [{title: "", index: 0}]) => {

        let convertedSongs = [];
        // Pour chaque musique à rechercher
        await Promise.all(songsToSearch.map(async (songToSearch, index) => {
            // On essai de convertir la musique
            const convertedSong = await this.#convertSong(songToSearch);
            if (convertedSong !== undefined) {
                // Si on en a trouvé une, on l'ajoute aux musiques converties puis supprime la musique de la recherche
                convertedSongs.push(convertedSong);
                songsToSearch[index] = undefined;
            } else {
                // Si on a pas trouvé, on réessaye
                const convertedSongAgain = await this.#convertSong(songToSearch);
                if (convertedSongAgain !== undefined) {
                    // Si on en a trouvé une, on l'ajoute aux musiques converties puis supprime la musique de la recherche
                    convertedSongs.push(convertedSongAgain);
                    songsToSearch[index] = undefined;
                }
            }
        }));

        // Ordonne les musiques dans le même ordre que la playlist originale
        convertedSongs.sort(function (a, b) {
            return a.index - b.index
        });

        return convertedSongs.map(convSong => {
            let song = {
                service: convSong.service,
                title: convSong.title,
                artist: convSong.artist,
                url: convSong.url,
                length: convSong.length,
                image: convSong.image,
                duration: Utils.getSongDuration(convSong.length)
            };
            return new Song(song);
        })
    }

}

module.exports = ServicePlaylist;