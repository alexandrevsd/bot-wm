const SongsList = require('./SongsList');
const MusicService = require("../services/MusicService");

class Search extends SongsList {

    service;
    keywords;
    limit;

    async set(service, query, limit) {
        this.service = service;
        this.keywords = query;
        this.limit = limit;
        await this.#getSearch();
    }

    async #getSearch() {
        const songs = await MusicService.search(this.service, this.keywords, this.limit);
        this.reset();
        songs.forEach(song => this.addSong(song));
    }

}

module.exports = Search;