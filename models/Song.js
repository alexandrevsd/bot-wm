const Utils = require("../Utils");
const ytdl = require('ytdl-core');
const scdl = require('soundcloud-downloader').default;

class Song {

    id;
    service;
    artist;
    title;
    length;
    duration;
    url;
    index;
    image;

    constructor(song) {
        if(song) {
            this.id = song.id;
            this.service = song.service;
            this.title = song.title;
            this.artist = song.artist;
            this.length = parseInt(song.length);
            this.duration = Utils.getSongDuration(this.length);
            this.url = song.url;
            this.index = song.index;
            this.image = song.image;
        } else {
            this.service = 'youtube';
            this.title = '';
            this.artist = '';
            this.length = 0;
            this.duration = Utils.getSongDuration(0);
            this.url = '';
            this.image = '';
        }
    }

    async doesExists() {
        let exists;
        const {type, service} = await Utils.musicLinkParser(this.url);
        this.service = service;
        if (type === 'song') {
            if (service === 'youtube') {
                try {
                    const infos = (await ytdl.getBasicInfo(this.url)).videoDetails;
                    const {title, artist} = Utils.formatSongInfos(infos.title, infos.author.name);
                    console.log(title)
                    console.log(artist)
                    this.url = infos.video_url;
                    this.title = title;
                    this.artist = artist;
                    this.image = infos.thumbnails[0].url;
                    this.length = Utils.getSongLength(service, infos.lengthSeconds);
                    this.duration = Utils.getSongDuration(this.length);
                    exists = true;
                } catch {
                    exists = false;
                }
            } else if(service === 'soundcloud') {
                try {
                    const infos = (await scdl.getInfo(this.url));
                    infos.artist = infos.publisher_metadata && infos.publisher_metadata.artist ? infos.publisher_metadata.artist : infos.user.username;
                    const {title, artist} = Utils.formatSongInfos(infos.title, infos.artist);
                    this.url = infos.permalink_url;
                    this.title = title;
                    this.artist = artist;
                    this.image = infos.artwork_url;
                    this.length = Utils.getSongLength(service, infos.duration);
                    this.duration = Utils.getSongDuration(this.length);
                    exists = true;
                } catch {
                    exists = false;
                }
            }
        }
        return exists;
    }

    getInfos() {

    }

}

module.exports = Song;