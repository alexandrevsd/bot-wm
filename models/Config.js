class Config {

    serverId;
    prefix;
    language;
    defaultVolume;
    deleteMessageOnCommand;
    
    constructor(config) {
        this.serverId = config.serverId;
        this.prefix = config.prefix;
        this.language = config.language;
        this.defaultVolume = config.defaultVolume;
        this.deleteMessageOnCommand = config.deleteMessageOnCommand;
    }

}

module.exports = Config;