const Voice = require("./Voice");
const BotService = require("../services/BotService");

class Bot {

    app;
    id;
    config;
    voice;
    permissions;
    playlists;

    constructor(app, id) {
        this.app = app;
        this.id = id;
        this.#loadConfig().then(config => {
            this.config = config;
            this.#loadPermissions().then(permissions => {
                this.permissions = permissions;
                this.#loadPlaylists().then(playlists => {
                    this.playlists = playlists;
                    this.voice = new Voice(this);
                })
            })
        });
    }

    send(message, content, icons = ['musical_note', 'robot']) {
        return message.channel.send(this.#iconsSurround(content, icons));
    }

    async reply(message, content, icons = ['musical_note', 'robot']) {
        return await this.send(message, `<@${message.member.id}>, ${content}`, icons);
    }

    async edit(messageToEdit, content, icons) {
        return await messageToEdit.edit(this.#iconsSurround(content, icons));
    }

    getGuild() {
        return this.app.client.guilds.cache.get(this.id);
    }

    #iconsSurround = (content, icons) => {
        icons.forEach(icon => content = `:${icon}: ${content} :${icon}:`);
        return content;
    }

    #loadConfig = async () => {
        return await BotService.getConfig(this.id);
    }

    #loadPermissions = async () => {
        const permissionsRoles = new Map();
        const permissions = await BotService.getPermissions(this.id);
        permissions.forEach(permission => {
            let roles = [];
            if(permissionsRoles.has(permission.name)) roles = permissionsRoles.get(permission.name);
            roles.push(permission.roleId);
            permissionsRoles.set(permission.name, roles);
        });
        return permissionsRoles;
    }

    #loadPlaylists = async () => {
        const playlists = new Map();
        const savedPlaylists = await BotService.getPlaylists(this.id);
        savedPlaylists.forEach(savedPlaylist => playlists.set(savedPlaylist.id, savedPlaylist));
        return playlists;
    }

}

module.exports = Bot;