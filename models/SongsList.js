const Song = require("./Song");

class SongsList {

    songs;

    constructor() {
        this.reset();
    }

    addSong(song) {
        this.songs.push(song);
    }

    addSongs(songs) {
        songs.forEach(song => this.addSong(song));
    }

    deleteSong() {

    }

    getSong(index) {
        return this.songs[index];
    }

    reset() {
        this.songs = [];
    }

    isEmpty() {

    }

}

module.exports = SongsList;