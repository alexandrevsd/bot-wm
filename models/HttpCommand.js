class HttpCommand {

    name;
    permissions;

    retrieveInfos(req, res, httpServer, callback) {
        // Récupère les informations envoyés dans axios
        const {serverId, memberId, userId, messageChannelId, messageId, args} = req.body;

        // Récupère le serveur lié à l'id du serveur et le membre qui a activé la commande
        const bot = httpServer.app.bots.get(serverId);
        const membersCache = bot.getGuild().members.cache;
        let member;
        if(memberId !== undefined) {
            member = membersCache.get(memberId);
        } else {
            member = membersCache.find(member => member.user.id === userId);
        }

        // Vérifies si le membre peut effectuer la commande
        const allowed = this.#allowed(bot, member);

        // Exécute la commande si le membre est autorisé
        if (allowed) {
            let message;

            // Supprime le message qui a été envoyé si nécessaire
            if (messageChannelId && messageId) {
                const channel = bot.getGuild().channels.cache.get(messageChannelId);
                message = channel.messages.cache.get(messageId);
                if (bot.config.deleteMessageOnCommand) {
                    if(message.deletable && !message.deleted) message.delete();
                }
            }
            callback({bot, member, args, message});
        } else res.json({
            code: 403
        });
    }

    #allowed(bot, member) {
        let allowed = true;
        let hasFullRights = false;

        // Pour chaque permission nécessaire à la commande
        this.permissions.forEach(permission => {
            const serverPerms = bot.permissions;
            const memberRoles = member.roles.cache.map((value, key) => key);

            // Vérifies si l'utilisateur a tous les droits
            if (serverPerms.has('*')) {
                const fullRightPerm = serverPerms.get('*')
                if (fullRightPerm.some(r => memberRoles.indexOf(r) >= 0)) hasFullRights = true;
            }

            if (!hasFullRights) {
                // Vérifies que le serveur a la permission de configurée
                if (serverPerms.has(permission)) {
                    const serverPerm = serverPerms.get(permission);
                    // Vérifies que le membre fait partie du groupe configuré
                    if (!serverPerm.some(r => memberRoles.indexOf(r) >= 0)) allowed = false;
                } else {
                    // Si il ne fait pas parti ne serait-ce que d'un seul groupe, on interdit l'accès
                    allowed = false;
                }
            }
        })
        return allowed;
    }

    async execute(req, res, httpServer) {

    }

}

module.exports = HttpCommand;