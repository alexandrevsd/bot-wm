const express = require('express');
const session = require('express-session');
const cors = require('cors');
const bodyParser = require("body-parser");
const {readdirSync} = require('fs');

const Utils = require("../Utils");

class HttpServer {

    app;
    #server;
    #port;

    constructor(app) {
        this.app = app;
        this.#server = express();
        this.#port = process.env.HTTP_PORT || 80;
        this.#middlewares();
        this.#listen();
    }

    #middlewares = () => {
        this.#server.use(cors());
        this.#server.use(session({
            secret: process.env.COOKIES_SECRET || 'NotSecuredAtAll',
            resave: false,
            saveUninitialized: true,
            cookie: {
                secure: true,
                maxAge: 1000 * 60 * 60 * 24 * 7,
                sameSite: true
            }
        }));
        this.#server.use(bodyParser.urlencoded({ extended: false }));
        this.#server.use(bodyParser.json());
        this.#loadRoutes();
    }

    #listen = () => {
        this.#server.listen(this.#port, () => {
            Utils.log(`listening at *:${this.#port}`, 'HTTP Server', 'yellow');
        })
    }

    #loadRoutes = () => {
        Utils.log(`loading all api routes...`, 'App', 'red');
        let commandsCounter = 0;
        const commandFolders = readdirSync('./api');
        for (const folder of commandFolders) {
            const commandFiles = readdirSync(`./api/${folder}`).filter(file => file.endsWith('.js'));
            for (const file of commandFiles) {
                const command = new (require(`../api/${folder}/${file}`));
                this.#server.post(`/api/${folder}/${command.name}`, (req, res) => command.execute(req, res, this));
                //Utils.log(`loading "${command.name}" api routes...`, 'App', 'red');
                commandsCounter++;
            }
        }
        Utils.log(`${commandsCounter} api commands loaded !`, 'App', 'red');
    }


}

module.exports = HttpServer;