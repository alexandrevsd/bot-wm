const SongsList = require("./SongsList");

class Queue extends SongsList {

    position;

    constructor() {
        super();
        this.position = 0;
    }

    getCurrentSong() {
        return this.getSong(this.position);
    }

    next() {
        if(this.hasNextSong()) {
            this.position++;
        } else {
            this.position = 0;
        }
    }

    previous() {
        if(this.hasPreviousSong()) {
            this.position--;
        } else {
            this.position = this.songs.length - 1;
        }
    }

    hasNextSong() {
        return this.songs.length > this.position + 1;
    }

    hasPreviousSong() {
        return this.position - 1 !== -1;
    }

    addPlaylist(playlist) {
        this.addSongs(playlist.songs);
    }

    addSongs(songs) {
        songs.forEach(song => this.addSong(song));
    }

}

module.exports = Queue;