const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class ConfigDao extends Model {
        static associate(models) {
        }
    };
    ConfigDao.init({
        serverId: {type: DataTypes.STRING, primaryKey: true, unique: true},
        prefix: DataTypes.STRING(1),
        language: DataTypes.STRING(2),
        defaultVolume: DataTypes.INTEGER(3),
        deleteMessageOnCommand: DataTypes.BOOLEAN
    }, {
        sequelize,
        timestamps: false,
        tableName: 'configs',
        modelName: 'ConfigDao',
    });
    return ConfigDao;
};