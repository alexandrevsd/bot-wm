const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class PlaylistDao extends Model {
        static associate(models) {
        }
    };
    PlaylistDao.init({
        serverId: {type: DataTypes.STRING},
        ownerId: {type: DataTypes.STRING},
        title: DataTypes.STRING,
        public: DataTypes.BOOLEAN,
        length: DataTypes.INTEGER,
        duration: DataTypes.STRING
    }, {
        sequelize,
        timestamps: false,
        tableName: 'playlists',
        modelName: 'PlaylistDao',
    });
    return PlaylistDao;
};