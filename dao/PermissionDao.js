'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class PermissionDao extends Model {
        static associate(models) {
        }
    };
    PermissionDao.init({
        serverId: {type: DataTypes.STRING, primaryKey: true},
        roleId: {type: DataTypes.STRING, primaryKey: true},
        name: {type: DataTypes.STRING, primaryKey: true}
    }, {
        sequelize,
        timestamps: false,
        tableName: 'permissions',
        modelName: 'PermissionDao',
    });
    return PermissionDao;
};