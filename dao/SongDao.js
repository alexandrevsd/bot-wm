const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class SongDao extends Model {
        static associate(models) {
        }
    };
    SongDao.init({
        playlistId: DataTypes.STRING,
        artist: DataTypes.STRING,
        title: DataTypes.STRING,
        length: DataTypes.STRING,
        service: DataTypes.STRING,
        url: DataTypes.STRING,
        image: DataTypes.STRING,
        position: DataTypes.INTEGER
    }, {
        sequelize,
        timestamps: false,
        tableName: 'songs',
        modelName: 'SongDao',
    });
    return SongDao;
};