const chalk = require("chalk");
const axios = require("axios");
const ytpl = require("ytpl");

class Utils {

    static getTitle(fromTitle) {
        let title = fromTitle;
        if (fromTitle.includes(' - ')) title = fromTitle.split(' - ')[1];
        return title;
    }

    static getArtist(fromTitle, fromArtist) {
        let artist = fromArtist;
        if (fromTitle.includes(' - ')) {
            artist = fromTitle.split(' - ')[0];
        }
        artist = artist.replace(' - Topic', '');
        return artist;
    }

    static formatSongInfos(title, artist) {
        artist = this.getArtist(title, artist);
        title = this.getTitle(title);
        return {title, artist};
    }

    static log(messageToLog, category = undefined, color = undefined) {
        category = category || 'Client';
        color = color || 'blue';
        console.log(`[${new Date(Date.now()).toLocaleTimeString()}] ${chalk[color](`[${category}]`)} ${messageToLog}`);
    }

    static async api(route, bot, message, args) {
        if (message.member === undefined) {
            message = {
                member: {
                    id: message
                },
                channel: {}
            }
        }
        return (await axios.post(`http://localhost:8080/api${route}`, {
            serverId: bot.id,
            memberId: message.member.id,
            messageChannelId: message.channel.id,
            messageId: message.id,
            args
        })).data
    }

    static error(res, errorMessage) {
        res.json({
            code: 400,
            error: errorMessage
        })
    }

    static async musicLinkParser(url) {
        let service;
        let type = 'playlist';
        if (this.isYoutubeUrl(url)) {
            service = 'youtube';
            type = 'song';
            try {
                if (this.isYoutubePlaylist(url) && await ytpl.getPlaylistID(url)) {
                    type = 'playlist';
                }
            } catch {
                type = 'song';
            }
        } else if (this.isSoundcloudPlaylist(url)) {
            service = 'soundcloud';
        } else if (this.isSoundcloudUrl(url)) {
            service = 'soundcloud';
            type = 'song';
        } else if (this.isSpotifyPlaylist(url)) {
            service = 'spotify';
        } else if (this.isAppleMusicPlaylist(url)) {
            service = 'apple_music';
        } else if (this.isDeezerPlaylist(url)) {
            service = 'deezer';
        }
        return {service, type};
    }

    static isUrl(url) {
        return typeof url === 'string' && (url.startsWith('https://') || url.startsWith('http://'));
    }

    static isYoutubeUrl(url) {
        return this.isUrl(url) && (url.includes('youtube.com') || url.includes('youtu.be'));
    }

    static isYoutubePlaylist(url) {
        return this.isYoutubeUrl(url) && url.includes('list=');
    }

    static isSoundcloudUrl(url) {
        return this.isUrl(url) && url.includes('soundcloud.com');
    }

    static isSoundcloudPlaylist(url) {
        return this.isSoundcloudUrl(url) && url.includes('sets/') && !url.includes('?in=');
    }

    static isSpotifyPlaylist(url) {
        return this.isUrl(url) && url.includes('spotify.com') && url.includes('/playlist/');
    }

    static isAppleMusicPlaylist(url) {
        return this.isUrl(url) && url.includes('music.apple.com') && url.includes('/playlist/');
    }

    static isDeezerPlaylist(url) {
        return this.isUrl(url) && url.includes('deezer.com') && url.includes('/playlist/');
    }

    static getDeezerPlaylistID(urlToParse) {
        return urlToParse.split("playlist/")[1];
    }

    static getSongLength(service, length) {
        if (service === 'youtube' || service === 'vk') {
            length *= 1000;
        }

        if (service === 'formatted') {
            console.log(length)
            const values = length.split(':').reverse();
            length = 0;
            values.forEach((value, index) => {
                length += (index > 0 ? parseInt(value) * (60 * (index)) : parseInt(value)) * 1000;
            })
        }

        return length;
    }

    static getSongDuration(length) {
        const date = new Date(length);
        let hours = date.getUTCHours() < 10 ? '0' + date.getUTCHours() : date.getUTCHours();
        const minutes = date.getUTCMinutes() < 10 ? '0' + date.getUTCMinutes() : date.getUTCMinutes();
        const seconds = date.getUTCSeconds() < 10 ? '0' + date.getUTCSeconds() : date.getUTCSeconds()
        hours = hours === '00' || hours === 0 ? '' : hours + ':';
        return hours + minutes + ':' + seconds;
    }


    static capitalize(s) {
        if (typeof s !== 'string') return '';
        return s.charAt(0).toUpperCase() + s.slice(1);
    }
}

module.exports = Utils;