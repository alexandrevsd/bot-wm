const assert = require('assert');
const BotService = require("../services/BotService");
const Utils = require("../Utils");

describe('TestBotService', function() {

    describe('Config', function() {
        it('Should create a config, get it, edit it and delete it', async function() {
            // Create
            let config = await BotService.createConfig('0000011111');
            assert.strictEqual(config.serverId, '0000011111');
            assert.strictEqual(config.language, 'EN');
            assert.strictEqual(config.defaultVolume, 5);
            assert.strictEqual(config.deleteMessageOnCommand, true);
            Utils.log('Config creation', 'PASSED', 'green');
            // Get
            config = await BotService.getConfig('0000011111');
            assert.strictEqual(config.serverId, '0000011111');
            assert.strictEqual(config.language, 'EN');
            assert.strictEqual(config.defaultVolume, 5);
            assert.strictEqual(config.deleteMessageOnCommand, true);
            Utils.log('Config found', 'PASSED', 'green');
            // Edit
            config.language = 'FR';
            config.defaultVolume = 10;
            config.deleteMessageOnCommand = false;
            config = await BotService.saveConfig(config);
            assert.strictEqual(config.serverId, '0000011111');
            assert.strictEqual(config.language, 'FR');
            assert.strictEqual(config.defaultVolume, 10);
            assert.strictEqual(config.deleteMessageOnCommand, false);
            Utils.log('Config edited', 'PASSED', 'green');
            // Delete
            await BotService.deleteConfig(config.serverId);
            config = await BotService.getConfig('0000011111');
            assert.strictEqual(config.defaultVolume, 5);
            await BotService.deleteConfig('0000011111');
            Utils.log('Config deleted', 'PASSED', 'green');
        });
    });

    describe('Permission', function() {
        it('Should create a permission, get it and delete it', async function() {
            // Create
            let permission = await BotService.createPermission('0000011111', '00001111222', 'move');
            assert.strictEqual(permission.serverId, '0000011111');
            assert.strictEqual(permission.roleId, '00001111222');
            assert.strictEqual(permission.name, 'move');
            Utils.log('Permission created', 'PASSED', 'green');
            // Get
            let permissions = await BotService.getPermissions('0000011111');
            assert.strictEqual(permissions[0].serverId, '0000011111');
            assert.strictEqual(permissions[0].roleId, '00001111222');
            assert.strictEqual(permissions[0].name, 'move');
            Utils.log('Permission found', 'PASSED', 'green');
            // Delete
            await BotService.deletePermission(permissions[0].serverId, permissions[0].roleId, permissions[0].name);
            permissions = await BotService.getPermissions('0000011111');
            assert.strictEqual(permissions[0], undefined);
            Utils.log('Permission deleted', 'PASSED', 'green');
        });
    });

    describe('Playlist', function() {
        it('Should create a playlist, get it, edit it and delete it', async function() {
            // Create
            let playlist = await BotService.createPlaylist('0000011111', '0000213121111222', 'maplaylist');
            assert.strictEqual(playlist.serverId, '0000011111');
            assert.strictEqual(playlist.ownerId, '0000213121111222');
            assert.strictEqual(playlist.name, 'maplaylist');
            Utils.log('Playlist created', 'PASSED', 'green');
            // Get
            let playlists = await BotService.getPlaylists('0000011111');
            assert.strictEqual(playlists[0].serverId, '0000011111');
            assert.strictEqual(playlists[0].ownerId, '0000213121111222');
            assert.strictEqual(playlists[0].name, 'maplaylist');
            assert.strictEqual(playlists[0].public, false);
            Utils.log('Playlist found', 'PASSED', 'green');
            // Edit
            playlists[0].name = 'enfaitecnon';
            playlists[0].public = true;
            playlist = await BotService.savePlaylist(playlists[0]);
            assert.strictEqual(playlist.name, 'enfaitecnon');
            assert.strictEqual(playlist.public, true);
            Utils.log('Playlist edited', 'PASSED', 'green');
            // Delete
            await BotService.deletePlaylist(playlist.id);
            playlists = await BotService.getPlaylists('0000011111');
            assert.strictEqual(playlists[0], undefined);
            Utils.log('Playlist deleted', 'PASSED', 'green');
        });
    });

});
