const ChatCommand = require('../../models/ChatCommand');
const Utils = require("../../Utils");

class Test extends ChatCommand {

    constructor() {
        super();
        this.name = 'test';
        this.aliases = ['t', 'te'];
    }

    async execute(bot, message, args, texts) {
        const result = await Utils.api('/misc/test', bot, message, args);
        if(result.code === 200) {
            message.reply('La commande a été executée sur le serveur : ' + bot.getGuild().name);
        }
    }

}

module.exports = Test;