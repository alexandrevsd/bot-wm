const ChatCommand = require('../../models/ChatCommand');
const Utils = require("../../Utils");

class Join extends ChatCommand {

    constructor() {
        super();
        this.name = 'join';
    }

    async execute(bot, message, args, texts) {
        const result = await Utils.api('/misc/join', bot, message, args);
        if(result.code === 200) {
            bot.send(message, 'Je suis entré dans le salon vocal "' + result.channelName + '"');
        } else if(result.code === 400) {
            if(result.error === 'memberVoiceNotConnected') {
                bot.reply(message, 'tu ne te trouves dans aucun salon vocal');
            } else if(result.error === 'alreadyInThisVoiceChannel') {
                bot.reply(message, 'je suis déjà dans ton salon vocal');
            } else if(result.error === 'voiceChannelNotFound') {
                bot.reply(message, 'je n\'ai pas trouvé le salon vocal spécifié');
            }
        }
    }

}

module.exports = Join;