const ChatCommand = require('../../models/ChatCommand');
const Utils = require("../../Utils");

class Join extends ChatCommand {

    constructor() {
        super();
        this.name = 'leave';
    }

    async execute(bot, message, args, texts) {
        const result = await Utils.api('/misc/leave', bot, message, args);
        if(result.code === 200) {
            bot.send(message, 'J\'ai quitté le salon vocal "' + result.channelName + '"');
        } else if(result.code === 400 && result.error === 'noVoiceConnection') {
            bot.reply(message, 'je ne suis connecté à aucun salon vocal');
        }
    }

}

module.exports = Join;