const ChatCommand = require('../../models/ChatCommand');
const Utils = require("../../Utils");

class Config extends ChatCommand {

    constructor() {
        super();
        this.name = 'config';
    }

    async execute(bot, message, args, texts) {
        const result = await Utils.api('/misc/config', bot, message, args);
        if(result.code === 200) {
            bot.send(message, "J'ai mis à jour le paramètre `" + result.parameterName + "` à `" + result.value + "`", ['wrench', 'robot']);
        } else if(result.code === 400) {
            if(result.error === 'notEnoughArgs') {
                bot.reply(message, 'tu dois préciser un paramètre à modifier et une valeur', ['x', 'robot']);
            } else if(result.error === 'prefixTooLongOrShort') {
                bot.reply(message, 'le préfixe des commandes doit-être 1 caractère', ['x', 'robot']);
            } else if(result.error === 'languageTooLongOrShort') {
                bot.reply(message, 'le language doit-être 2 caractères', ['x', 'robot']);
            } else if(result.error === 'defaultVolumeNotCorrect') {
                bot.reply(message, 'le volume par défaut doit-être entre 0 et 100', ['x', 'robot']);
            } else if(result.error === 'deleteMessageOnCommandNotBoolean') {
                bot.reply(message, 'la suppression des messages doit-être sur `true` ou `false`', ['x', 'robot']);
            } else if(result.error === 'parameterNotFound') {
                bot.reply(message, 'le paramètre spécifié n\'existe pas', ['x', 'robot']);
            }
        }
    }

}

module.exports = Config;