const ChatCommand = require('../../models/ChatCommand');
const Utils = require("../../Utils");

class Soundcloud extends ChatCommand {

    constructor() {
        super();
        this.name = 'soundcloud';
        this.aliases = ['sc'];
    }

    async execute(bot, message, args, texts) {
        let messageToEdit;

        messageToEdit = await bot.send(message, 'Recherche de la musique sur Soundcloud...', ['mag', 'musical_note']);

        const result = await Utils.api('/music/search', bot, message, ['soundcloud', '#play#'].concat(args));

        if (result.code === 200) {
            if (result.played) bot.edit(messageToEdit, "J'ai lancé `" + result.song.title + "`", ['arrow_forward', 'musical_note']);
            else bot.edit(messageToEdit, "J'ai ajouté `" + result.song.title + "` à la file d'attente", ['arrow_right_hook', 'musical_note']);
        } else {
            if (result.error === 'memberVoiceNotConnected') {
                bot.reply(message, "tu n'est pas dans un salon vocal", ['x', 'musical_note'])
            } else if (result.error === 'noSearchQuery') {
                bot.reply(message, "tu dois me donner les mots-clés à rechercher", ['x', 'musical_note'])
            } else if (result.error === 'searchResultNotFound') {
                bot.reply(message, "le résultat que tu m'as demandé n'existe pas", ['x', 'musical_note'])
            }
        }
    }

}

module.exports = Soundcloud;