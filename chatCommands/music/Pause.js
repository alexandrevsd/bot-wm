const ChatCommand = require('../../models/ChatCommand');
const Utils = require("../../Utils");

class Pause extends ChatCommand {

    constructor() {
        super();
        this.name = 'pause';
        this.aliases = ['pa'];
    }

    async execute(bot, message, args, texts) {
        const result = await Utils.api('/music/pause', bot, message, args);
        if(result.code === 200) {
            bot.send(message, "J'ai mis la musique sur pause", ['pause_button', 'musical_note']);
        }
        if(result.code === 400) {
            if(result.error === 'memberVoiceNotConnected') {
                bot.reply(message, "tu n'est pas dans un salon vocal", ['x', 'musical_note'])
            } else if(result.error === 'botVoiceNotConnected') {
                bot.reply(message, "je ne suis pas dans un salon vocal", ['x', 'musical_note'])
            } else if(result.error === 'musicNotPlaying') {
                bot.reply(message, "aucune musique en cours de lecture", ['x', 'musical_note'])
            } else if(result.error === 'alreadyPaused') {
                bot.reply(message, "la musique est déjà sur pause", ['x', 'musical_note'])
            }
        }
    }

}

module.exports = Pause;