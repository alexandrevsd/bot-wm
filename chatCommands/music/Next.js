const ChatCommand = require('../../models/ChatCommand');
const Utils = require("../../Utils");

class Next extends ChatCommand {

    constructor() {
        super();
        this.name = 'next';
        this.aliases = ['n', 'skip'];
    }

    async execute(bot, message, args, texts) {
        const result = await Utils.api('/music/next', bot, message, args);

        if(result.code === 200) {
            bot.send(message, 'Je suis passé à `' + result.song.title + '`', ['next_track', 'musical_note']);
        } else if(result.code === 400) {
            if(result.error === 'memberVoiceNotConnected') {
                bot.reply(message, "tu n'est pas dans un salon vocal", ['x', 'musical_note'])
            } else if(result.error === 'botVoiceNotConnected') {
                bot.reply(message, "je ne suis pas dans un salon vocal", ['x', 'musical_note'])
            } else if(result.error === 'musicNotPlaying') {
                bot.reply(message, "aucune musique en cours de lecture", ['x', 'musical_note'])
            } else if(result.error === 'noNextMusic') {
                bot.reply(message, "il n'y a pas de musique après", ['x', 'musical_note'])
            }
        }

    }

}

module.exports = Next;