const ChatCommand = require('../../models/ChatCommand');
const Utils = require("../../Utils");

class Next extends ChatCommand {

    constructor() {
        super();
        this.name = 'shuffle';
        this.aliases = ['random'];
    }

    async execute(bot, message, args, texts) {
        const result = await Utils.api('/music/shuffle', bot, message, args);

        if(result.code === 200) {
            if(result.random === true) {
                bot.send(message, 'J\'ai mélangé la file d\'attente actuelle', ['twisted_rightwards_arrows', 'musical_note']);
            } else {
                bot.send(message, 'J\'ai remis la file d\'attente en mode normal', ['twisted_rightwards_arrows', 'musical_note']);
            }
        } else if(result.code === 400) {
            if(result.error === 'memberVoiceNotConnected') {
                bot.reply(message, "tu n'est pas dans un salon vocal", ['x', 'musical_note'])
            } else if(result.error === 'botVoiceNotConnected') {
                bot.reply(message, "je ne suis pas dans un salon vocal", ['x', 'musical_note'])
            } else if(result.error === 'musicNotPlaying') {
                bot.reply(message, "aucune musique en cours de lecture", ['x', 'musical_note'])
            }
        }

    }

}

module.exports = Next;