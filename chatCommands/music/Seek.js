const ChatCommand = require('../../models/ChatCommand');
const Utils = require("../../Utils");

class Seek extends ChatCommand {

    constructor() {
        super();
        this.name = 'seek';
        this.aliases = ['sk'];
    }

    async execute(bot, message, args, texts) {
        const result = await Utils.api('/music/seek', bot, message, args);
        if(result.code === 200) {
            if(result.seeked > 0) {
                bot.send(message, 'J\'ai avancé de `' + result.seeked + '` secondes', ['fast_forward', 'musical_note']);
            } else {
                bot.send(message, 'J\'ai reculé de `' + result.seeked.toString().replace('-', '') + '` secondes', ['rewind', 'musical_note']);
            }
        } else if(result.code === 400) {
            if(result.error === 'memberVoiceNotConnected') {
                bot.reply(message, "tu n'est pas dans un salon vocal", ['x', 'musical_note'])
            } else if(result.error === 'botVoiceNotConnected') {
                bot.reply(message, "je ne suis pas dans un salon vocal", ['x', 'musical_note'])
            } else if(result.error === 'musicNotPlaying') {
                bot.reply(message, "aucune musique en cours de lecture", ['x', 'musical_note'])
            } else if(result.error === 'seekNotANumber') {
                bot.reply(message, "tu peux avancer de `x` ou `-x` secondes", ['x', 'musical_note'])
            }
        }
    }

}

module.exports = Seek;