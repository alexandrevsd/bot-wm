const ChatCommand = require('../../models/ChatCommand');
const Utils = require("../../Utils");
const {MessageEmbed} = require("discord.js");


/** Creates a message embed **/
function newEmbed(service, keywords) {
    const embed = new MessageEmbed();
    embed.setTitle(":musical_note: :mag: Recherche `" + Utils.capitalize(service) + "` pour `" + keywords + "` :mag: :musical_note:");
    embed.setFooter("!se 1, 2, 3... pour choisir la musique à jouer ou à ajouter à la file d'attente");
    return embed;
}

/** Fills a message embed with found songs **/
function getResultsEmbed(bot, service, keywords) {
    const embed = newEmbed(service, keywords);
    const foundSongs = bot.voice.search.songs;
    foundSongs.forEach((song, index) => embed.addField('N°' + (index + 1) + ' (' + song.length + ') | ' + song.title, song.url));
    return embed;
}

class Search extends ChatCommand {

    constructor() {
        super();
        this.name = 'search';
        this.aliases = ['se'];
    }

    async execute(bot, message, args, texts) {
        let messageToEdit;
        if (isNaN(args[0]) || args.length > 1) messageToEdit = await bot.send(message, 'Recherche de musiques sur les plateformes...', ['mag', 'musical_note']);
        const result = await Utils.api('/music/search', bot, message, args);
        if (result.code === 200) {
            if (result.song) {
                if (result.played) bot.send(message, "J'ai lancé `" + result.song.title + "`", ['arrow_forward', 'musical_note']);
                else bot.send(message, "J'ai ajouté `" + result.song.title + "` à la file d'attente", ['arrow_right_hook', 'musical_note']);
            } else {
                messageToEdit.edit('', {embed: getResultsEmbed(bot, bot.voice.search.service, bot.voice.search.keywords)});
            }
        } else {
            if(result.error === 'memberVoiceNotConnected') {
                bot.reply(message, "tu n'est pas dans un salon vocal", ['x', 'musical_note'])
            } else if(result.error === 'noSearchQuery') {
                bot.reply(message, "tu dois me donner les mots-clés à rechercher", ['x', 'musical_note'])
            } else if(result.error === 'searchResultNotFound') {
                bot.reply(message, "le résultat que tu m'as demandé n'existe pas", ['x', 'musical_note'])
            }
        }
    }

}

module.exports = Search;