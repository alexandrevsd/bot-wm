const ChatCommand = require('../../models/ChatCommand');
const Utils = require("../../Utils");

class Lyrics extends ChatCommand {

    constructor() {
        super();
        this.name = 'lyrics';
    }

    async execute(bot, message, args, texts) {
        const result = await Utils.api('/music/lyrics', bot, message, args);

        if(result.code === 200) {
            bot.send(message, result.lyrics.substr(0, 1500));
        }

    }

}

module.exports = Lyrics;