const ChatCommand = require('../../models/ChatCommand');
const Utils = require("../../Utils");
const Resume = require("./Resume");

class Play extends ChatCommand {

    constructor() {
        super();
        this.name = 'play';
        this.aliases = ['p'];
    }

    async execute(bot, message, args, texts) {
        if (args.length === 0) return new Resume().execute(bot, message, args, texts);
        let messageToEdit;
        if (Utils.isUrl(args[0])) {
            if (await Utils.musicLinkParser(args[0]).type === 'playlist') {
                messageToEdit = await bot.send(message, 'Je récupère la playlist sur les plateformes...', ['mag', 'musical_note']);
            } else {
                messageToEdit = await bot.send(message, 'Je recherche la musique sur les plateformes...', ['mag', 'musical_note']);
            }
        } else {
            messageToEdit = await bot.send(message, 'Je récupère la playlist dans la base de données...', ['mag', 'musical_note']);
        }
        const result = await Utils.api('/music/play', bot, message, args);
        if (result.code === 200) {
            if (result.played) {
                bot.edit(messageToEdit, 'J\'ai lancé `' + result.song.title + '`', ['arrow_forward', 'musical_note']);
            } else {
                if(result.song) {
                    bot.edit(messageToEdit, 'J\'ai ajouté `' + result.song.title + '` à la file d\'attente', ['arrow_right_hook', 'musical_note']);
                } else {
                    const service = result.playlist.service ? Utils.capitalize(result.playlist.service) + ' : ' : '';
                    bot.edit(messageToEdit, 'J\'ai ajouté la playlist `' + service + result.playlist.title + '` à la file d\'attente', ['arrow_right_hook', 'musical_note']);
                }
            }
        } else if (result.code === 400) {
            if (result.error === 'memberVoiceNotConnected') {
                bot.reply(message, 'tu n\'es pas dans un salon vocal', ['x', 'musical_note']);
            } else if (result.error === 'songDoesNotExists') {
                bot.reply(message, 'la musique n\'existe pas', ['x', 'musical_note']);
            } else if (result.error === 'playlistDoesNotExists') {
                bot.reply(message, 'la playlist n\'existe pas', ['x', 'musical_note']);
            } else if (result.error === 'notYoutubeOrSoundcloudPlaylist') {
                bot.reply(message, 'je prends uniquement en charge les playlist Youtube et Soundcloud', ['x', 'musical_note']);
            }
        }
    }

}

module.exports = Play;