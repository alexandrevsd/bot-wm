const ChatCommand = require('../../models/ChatCommand');
const Utils = require("../../Utils");

class Loop extends ChatCommand {

    constructor() {
        super();
        this.name = 'loop';
        this.aliases = ['replay'];
    }

    async execute(bot, message, args, texts) {
        const result = await Utils.api('/music/loop', bot, message, args);

        if(result.code === 200) {
            if(result.loop === true) {
                bot.send(message, 'La file d\'attente joue maintenant en boucle', ['twisted_rightwards_arrows', 'musical_note']);
            } else {
                bot.send(message, 'La file d\'attente ne joue plus en boucle', ['twisted_rightwards_arrows', 'musical_note']);
            }
        } else if(result.code === 400) {
            if(result.error === 'memberVoiceNotConnected') {
                bot.reply(message, "tu n'est pas dans un salon vocal", ['x', 'musical_note'])
            } else if(result.error === 'botVoiceNotConnected') {
                bot.reply(message, "je ne suis pas dans un salon vocal", ['x', 'musical_note'])
            } else if(result.error === 'musicNotPlaying') {
                bot.reply(message, "aucune musique en cours de lecture", ['x', 'musical_note'])
            }
        }

    }

}

module.exports = Loop;