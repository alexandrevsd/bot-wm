const ChatCommand = require('../../models/ChatCommand');
const Utils = require("../../Utils");

class Next extends ChatCommand {

    constructor() {
        super();
        this.name = 'stop';
        this.aliases = ['s'];
    }

    async execute(bot, message, args, texts) {
        const result = await Utils.api('/music/stop', bot, message, args);

        if(result.code === 200) {
            bot.send(message, "J'ai arrêté la musique", ['stop_button', 'musical_note']);
        } else if(result.code === 400) {
            if(result.error === 'memberVoiceNotConnected') {
                bot.reply(message, "tu n'est pas dans un salon vocal", ['x', 'musical_note']);
            } else if(result.error === 'botVoiceNotConnected') {
                bot.reply(message, "je ne suis pas dans un salon vocal", ['x', 'musical_note']);
            } else if(result.error === 'musicNotPlaying') {
                bot.reply(message, "aucune musique en cours de lecture", ['x', 'musical_note']);
            }
        }

    }

}

module.exports = Next;