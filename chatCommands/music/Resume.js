const ChatCommand = require('../../models/ChatCommand');
const Utils = require("../../Utils");

class Resume extends ChatCommand {

    constructor() {
        super();
        this.name = 'resume';
        this.aliases = ['res'];
    }

    async execute(bot, message, args, texts) {
        const result = await Utils.api('/music/resume', bot, message, args);
        if(result.code === 200) {
            bot.send(message, "J'ai relancé la musique", ['arrow_forward', 'musical_note']);
        } else if(result.code === 400) {
            if(result.error === 'memberVoiceNotConnected') {
                bot.reply(message, "tu n'est pas dans un salon vocal", ['x', 'musical_note'])
            } else if(result.error === 'botVoiceNotConnected') {
                bot.reply(message, "je ne suis pas dans un salon vocal", ['x', 'musical_note'])
            } else if(result.error === 'musicNotPlaying') {
                bot.reply(message, "aucune musique en cours de lecture", ['x', 'musical_note'])
            } else if(result.error === 'musicNotPaused') {
                bot.reply(message, "la musique n'est pas sur pause", ['x', 'musical_note'])
            }
        }
    }

}

module.exports = Resume;