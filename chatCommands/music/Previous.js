const ChatCommand = require('../../models/ChatCommand');
const Utils = require("../../Utils");

class Previous extends ChatCommand {

    constructor() {
        super();
        this.name = 'previous';
        this.aliases = ['pr', 'before'];
    }

    async execute(bot, message, args, texts) {
        const result = await Utils.api('/music/previous', bot, message, args);
        if(result.code === 200) {
            bot.send(message, 'Je suis retourné à `' + result.song.title + '`', ['previous_track', 'musical_note']);
        } else if(result.code === 400) {
            if(result.error === 'memberVoiceNotConnected') {
                bot.reply(message, "tu n'est pas dans un salon vocal", ['x', 'musical_note'])
            } else if(result.error === 'botVoiceNotConnected') {
                bot.reply(message, "je ne suis pas dans un salon vocal", ['x', 'musical_note'])
            } else if(result.error === 'musicNotPlaying') {
                bot.reply(message, "aucune musique en cours de lecture", ['x', 'musical_note'])
            } else if(result.error === 'noPreviousMusic') {
                bot.reply(message, "il n'y a pas de musique avant", ['x', 'musical_note'])
            }
        }
    }

}

module.exports = Previous;