const ChatCommand = require('../../models/ChatCommand');
const Utils = require("../../Utils");

class Volume extends ChatCommand {

    constructor() {
        super();
        this.name = 'volume';
        this.aliases = ['vol', 'v'];
    }

    async execute(bot, message, args, texts) {
        const result = await Utils.api('/music/volume', bot, message, args);
        if(result.code === 200) {
            bot.send(message, "J'ai mis le volume à " + result.volume + "%", ['loud_sound', 'musical_note']);
        } else if(result.code === 400) {
            if(result.error === 'memberVoiceNotConnected') {
                bot.reply(message, "tu n'est pas dans un salon vocal", ['x', 'musical_note'])
            } else if(result.error === 'volumeNotCorrect') {
                bot.reply(message, "le volume doit être entre 0 et 100", ['x', 'musical_note'])
            }
        }
    }

}

module.exports = Volume;