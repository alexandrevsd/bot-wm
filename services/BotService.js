const dao = require('../dao');
const Permission = require("../models/Permission");
const Config = require("../models/Config");
const Playlist = require("../models/Playlist");
const Song = require("../models/Song");
const {ConfigDao, PermissionDao, PlaylistDao, SongDao} = dao;

class BotService {


    ///////////
    // SONGS //
    ///////////

    static async getSongs(playlistId) {
        const songs = await SongDao.findAll({
            where: {playlistId},
            order: [['position', 'ASC']]
        }) || [];
        return songs.map(song => new Song(song));
    }

    static async createSong(playlistId, songToCreate) {
        const song = await SongDao.create({
            playlistId,
            artist: songToCreate.artist,
            title: songToCreate.title,
            length: songToCreate.length,
            service: songToCreate.service,
            url: songToCreate.url,
            image: songToCreate.image
        });
        return new Song(song);
    }

    static async deleteSong(id) {
        const song = await SongDao.findOne({
            where: {id}
        });
        await song.destroy();
    }

    static async setSongIndex(id, index) {
        const song = await SongDao.findOne({
            where: {id}
        });
        song.position = index;
        await song.save();
    }


    ///////////////
    // PLAYLISTS //
    ///////////////

    static async createPlaylist(serverId, ownerId, title, publicValue, length) {
        const playlist = await PlaylistDao.create({
            serverId,
            ownerId,
            title,
            length,
            duration: '00:00',
            public: publicValue
        })
        return new Playlist(playlist);
    }

    static async getPlaylist(serverId, id) {
        const playlist =  await PlaylistDao.findOne({
            where: {id}
        }) || [];
        return new Playlist(playlist);
    }

    static async getPlaylists(serverId) {
        const playlists =  await PlaylistDao.findAll({
            where: {serverId}
        }) || [];
        return playlists.map(playlist => new Playlist(playlist));
    }

    static async savePlaylist(playlist) {
        const dbPlaylist = await PlaylistDao.findOne({
            where: {id: playlist.id}
        });
        dbPlaylist.name = playlist.name;
        dbPlaylist.public = playlist.public;
        dbPlaylist.length = playlist.length;
        return new Playlist(await dbPlaylist.save());
    }

    static async deletePlaylist(id) {
        const playlist = await PlaylistDao.findOne({
            where: {id}
        })
        await playlist.destroy();
    }


    ////////////
    // CONFIG //
    ////////////

    static async getConfig(serverId) {
        const config =  await ConfigDao.findOne({
            where: {serverId}
        });
        if(config === null) return await this.createConfig(serverId);
        else return new Config(config);
    }

    static async createConfig(serverId) {
        const config = await ConfigDao.create({
            serverId,
            prefix: '!',
            language: 'EN',
            defaultVolume: 5,
            deleteMessageOnCommand: true
        })
        return new Config(config);
    }

    static async saveConfig(config) {
        const dbConfig = await ConfigDao.findOne({
            where: {serverId: config.serverId}
        });
        dbConfig.prefix = config.prefix;
        dbConfig.language = config.language;
        dbConfig.defaultVolume = config.defaultVolume;
        dbConfig.deleteMessageOnCommand = config.deleteMessageOnCommand;
        return new Config(await dbConfig.save());
    }

    static async deleteConfig(serverId) {
        const config = await ConfigDao.findOne({
            where: {serverId}
        });
        await config.destroy();
    }


    /////////////////
    // PERMISSIONS //
    /////////////////

    static async getPermissions(serverId) {
        const permissions = await PermissionDao.findAll({
            where: {serverId}
        })
        return permissions.map(permission => new Permission(permission));
    }

    static async createPermission(serverId, roleId, name) {
        return await PermissionDao.create({
            serverId,
            roleId,
            name
        })
    }

    static async deletePermission(serverId, roleId, name) {
        const dbPermission = await PermissionDao.findOne({
            where: {
                serverId,
                roleId,
                name
            }
        });
        await dbPermission.destroy();
    }

}

module.exports = BotService;