const {searchOnMyFreeMp3} = require('vk-music-dl/lib');
const scdl = require('soundcloud-downloader').default
const ytsr = require('ytsr');

const Song = require("../models/Song");
const Utils = require("../Utils");

class MusicService {

    ////////////
    // SEARCH //
    ////////////

    static async search(service, query, limit) {
        let search;
        if (service === 'vk') {
            search = await this.searchOnVk(query, limit);
        } else if (service === 'youtube') {
            search = await this.searchOnYoutube(query, limit);
        } else {
            search = await this.searchOnSoundcloud(query, limit);
        }
        return search;
    }

    static async searchOnVk(query, limit) {
        const songs = [];
        const searchResults = await searchOnMyFreeMp3(query) || [];
        console.log(searchResults)
        searchResults.forEach(searchResult => songs.push(this.#searchResultToSong(searchResult, 'vk')));
        return songs;
    }

    static async searchOnYoutube(query, limit) {
        const songs = [];
        const filters = await ytsr.getFilters(query);
        const filter = filters.get('Type').get('Video');
        const searchResults = (await ytsr(filter.url, {limit})).items;
        searchResults.forEach(searchResult => {
            if(searchResult.duration !== null && searchResult.author) {
                songs.push(this.#searchResultToSong(searchResult, 'youtube'))
            }
        });
        return songs;
    }

    static async searchOnSoundcloud(query, limit) {
        const songs = [];
        const searchResults = (await scdl.search({query, limit, resourceType: 'tracks'})).collection;
        searchResults.forEach(searchResult => songs.push(this.#searchResultToSong(searchResult, 'soundcloud')));
        return songs;
    }

    static #searchResultToSong(searchResult, service) {
        let url, image, title, artist, formatService = service;

        if (service === 'soundcloud') {

            if (searchResult.publisher_metadata && searchResult.publisher_metadata.artist) {
                searchResult.artist = searchResult.publisher_metadata.artist;
            } else {
                searchResult.artist = searchResult.user.username;
            }
            const formatted = Utils.formatSongInfos(searchResult.title, searchResult.artist);
            title = formatted.title;
            artist = formatted.artist;
            url = searchResult['permalink_url'];
            image = searchResult['artwork_url'];

        } else if (service === 'youtube') {

            const formatted = Utils.formatSongInfos(searchResult.title, searchResult.author.name);
            title = formatted.title
            artist = formatted.artist;
            formatService = 'formatted';
            url = searchResult['url'];
            image = searchResult['bestThumbnail']['url'];

        } else {

            url = searchResult['url'].split('&long')[0];
            title = `${searchResult['artist']} ${searchResult['title']}`;
            image = searchResult['album'] && searchResult['album']['thumb'] && searchResult['album']['thumb']['photo_135'] || searchResult['album']['thumb']['photo_68'] || searchResult['album']['thumb']['photo_34'];

        }

        const song = {
            url,
            title,
            artist,
            service,
            length: Utils.getSongLength(formatService, searchResult['duration']),
            image
        }
        return new Song(song);
    }

}

module.exports = MusicService;