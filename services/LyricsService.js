const Genius = require("genius-lyrics");
const genius = new Genius.Client(process.env.GENIUS_TOKEN);
const fetch = require('node-fetch');

class LyricsService {

    static async get(title, artist) {
        let lyrics;
        if (artist === undefined) {
            lyrics = await this.getGoogleLyrics(title);
            if (!lyrics) lyrics = await this.getGeniusLyrics(title);
        } else {
            lyrics = await this.getGoogleLyrics(title, artist);
            if (!lyrics) lyrics = await this.getGeniusLyrics(title, artist);
        }
        return lyrics;
    }

    static async getGoogleLyrics(title = '', artist = '') {
        const delimiter1 = '</div></div></div></div><div class="hwc"><div class="BNeawe tAd8D AP7Wnd"><div><div class="BNeawe tAd8D AP7Wnd">';
        const delimiter2 = '</div></div></div></div></div><div><span class="hwc"><div class="BNeawe uEec3 AP7Wnd">';
        const url = "https://www.google.com/search?q=";
        let lyrics;
        let success = false;
        if (artist !== '') artist = artist + ' ';
        title = title.split('(')[0].split('[')[0].split('|')[0];
        try {
            lyrics = await fetch(`${url}${encodeURIComponent(artist + title)}+lyrics`);
            lyrics = await lyrics.textConverted();
            [, lyrics] = lyrics.split(delimiter1);
            [lyrics] = lyrics.split(delimiter2);
            success = true;
        } catch {
        }
        if (success) {
            const split = lyrics.split('\n');
            var final = '';
            for (var i = 0; i < split.length; i++) {
                final = `${final}${split[i]}\n`;
            }
        }
        return (final) ? final.trim() : undefined;
    }

    static async getGeniusLyrics(title = '', artist = '') {
        title = title.split('(')[0].split('[')[0].split('|')[0];

        let firstSong;

        try {
            const searches = await genius.songs.search(title);
            const results = [];
            searches.forEach(result => {
                const testResultName = result.artist.name.toLowerCase();
                const testArtistName = artist.toLowerCase();
                if (testResultName.includes(testArtistName) || testArtistName.includes(testResultName)) {
                    results.push(result);
                }
            })
            firstSong = results[0];
            return (firstSong) ? await firstSong.lyrics() : undefined;
        } catch {
        }
        return undefined;
    }

}

module.exports = LyricsService;