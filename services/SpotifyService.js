const fetch = require('node-fetch');
const base64 = require('base-64');

class SpotifyService {

    #clientId;
    #clientSecret;
    #token;
    #expiresAt;

    constructor() {
        this.#clientId = process.env.SPOTIFY_CLIENT_ID;
        this.#clientSecret = process.env.SPOTIFY_CLIENT_SECRET;
    }

    #getToken = async () => {
        const body = new URLSearchParams();
        body.append('grant_type', 'client_credentials');
        const response = await fetch('https://accounts.spotify.com/api/token', {
            method: 'POST',
            headers: {
                'Authorization': 'Basic ' + base64.encode(this.#clientId + ':' + this.#clientSecret)
            },
            body
        })
        const result = await response.json();
        this.#expiresAt = Date.now() + result['expires_in'];
        this.#token = result['access_token'];
    }

    #isLoggedIn = () => {
        return this.#token !== undefined && this.#expiresAt > Date.now() + 2
    }

    #login = async () => {
        if (!this.#isLoggedIn()) {
            await this.#getToken();
        }
    }

    #getData = async (url) => {
        await this.#login();
        const response = await fetch(url, {
            method: 'GET',
            headers: {
                'Authorization': 'Bearer ' + this.#token
            }
        })
        return await response.json();
    }

    async getPlaylistTracks(playlistId) {
        const tracks = [];
        let countWhile = 0;
        do {
            const result = await this.#getData(`https://api.spotify.com/v1/playlists/${playlistId}/tracks?offset=${countWhile * 100}&limit=100`);
            tracks.push(...(result['items'].map(entry => entry['track'])))
            countWhile++;
        } while (tracks.length === countWhile * 100);
        return tracks;
    }

    checkPlaylistUrl(url) {
        return (url.startsWith('http://') || url.startsWith('https://')) && url.includes('spotify.com/') && url.includes('/playlist/') && url.split('playlist/')[1].length > 0
    }

    getPlaylistId(url) {
        return url.split('playlist/')[1];
    }

}

module.exports = SpotifyService;